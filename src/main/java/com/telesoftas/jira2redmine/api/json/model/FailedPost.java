package com.telesoftas.jira2redmine.api.json.model;

import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FailedPost {

    TimeEntry timeEntry;
    String exception;

}
