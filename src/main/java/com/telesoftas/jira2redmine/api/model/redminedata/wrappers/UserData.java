package com.telesoftas.jira2redmine.api.model.redminedata.wrappers;

import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;
import lombok.Data;

import java.util.List;

@Data
public class UserData extends Metadata {

    private List<RedmineUser> users;

}
