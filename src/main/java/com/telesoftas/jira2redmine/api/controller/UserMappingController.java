package com.telesoftas.jira2redmine.api.controller;

import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;
import com.telesoftas.jira2redmine.api.model.userMapping.JiraUser;
import com.telesoftas.jira2redmine.api.repository.UserRepository;
import com.telesoftas.jira2redmine.api.service.user.RedmineUserService;
import com.telesoftas.jira2redmine.api.service.user.mapping.UserMapping;
import com.telesoftas.jira2redmine.api.service.user.mapping.UserMappingService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

@Controller
public class UserMappingController {

    private final UserMapping userMapping;
    private final RedmineUserService userService;
    private final UserRepository userRepository;

    public UserMappingController(UserMappingService userMapping,
                                 RedmineUserService userService,
                                 UserRepository userRepository) {
        this.userMapping = userMapping;
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @GetMapping("/user/check")
    public RedirectView determineIfUnmappedUsersExist(@ModelAttribute("from") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate from,
                                                      @ModelAttribute("to") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate to,
                                                      RedirectAttributes attributes) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Map<JiraUser, RedmineUser> jiraWorkLogs = userMapping.getMappedUsers(from, to);
        attributes.addAttribute("from", from.format(formatter));
        attributes.addAttribute("to", to.format(formatter));
        if (jiraWorkLogs.containsValue(RedmineUser.builder().build())) {
            attributes.addFlashAttribute("jiraWorkLogs", jiraWorkLogs);
            return new RedirectView("/user/map");
        }
        return new RedirectView("/timeEntries/import");
    }

    @GetMapping("/user/map")
    public String showUserMapping(@ModelAttribute("from") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate from,
                                  @ModelAttribute("to") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate to,
                                  @ModelAttribute("jiraWorkLogs") Map<JiraUser, RedmineUser> jiraWorkLogs,
                                  Model model) {
        model.addAttribute("unmapped", userMapping.getUnmappedUsers(jiraWorkLogs));
        model.addAttribute("mapped", userMapping.getMappedUsers(jiraWorkLogs));
        model.addAttribute("userList", getRedmineUsers());
        return "user/userMapping";
    }

    private Collection<RedmineUser> getRedmineUsers() {
        Collection<RedmineUser> userList = new ArrayList<>();
        userList.add(RedmineUser.builder().build());
        userList.addAll(userService.getUserList());
        return userList;
    }

    @GetMapping("/user/manage")
    public String getUserMapping(Model model) {
        model.addAttribute("mappedUserList", userRepository.findAll());

        model.addAttribute("allUsers", userService.getUserList());
        return "user/userManagement";
    }
}
