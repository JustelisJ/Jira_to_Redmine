package com.telesoftas.jira2redmine.api.mapper;

import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;
import com.telesoftas.jira2redmine.api.service.user.UserService;
import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.Map;

@Log4j2
public class JiraUsersToRedmineUsersMapper {

    private UserService userService;

    public JiraUsersToRedmineUsersMapper(UserService userService) {
        this.userService = userService;
    }

    public Map<String, RedmineUser> mapJiraUsersToRedmineUsersId(Map<String, String> jiraAccountDetails) {
        Map<String, RedmineUser> mappedUsersToAccountId = new HashMap<>();
        jiraAccountDetails.forEach((accountId, displayName) -> userService.findUserByName(displayName).ifPresentOrElse(
                (user -> mappedUsersToAccountId.put(accountId, user)),
                (() -> log.error("Couldn't find RedmineUser with name " + displayName))));

        return mappedUsersToAccountId;
    }
}
