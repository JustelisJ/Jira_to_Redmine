package com.telesoftas.jira2redmine.api.repository;

import com.telesoftas.jira2redmine.api.model.imports.ImportStatus;
import com.telesoftas.jira2redmine.api.model.imports.ImportStatusTableStatus;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface ImportStatusRepository extends CrudRepository<ImportStatus, Long> {

    @Transactional
    @Modifying
    @Query("update ImportStatus u set u.status = :status where u.sessionID = :sessionID")
    int updateStatus(@Param("sessionID") Long sessionID, @Param("status") ImportStatusTableStatus status);

}
