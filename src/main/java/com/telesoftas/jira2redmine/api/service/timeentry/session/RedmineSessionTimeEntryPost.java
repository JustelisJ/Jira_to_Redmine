package com.telesoftas.jira2redmine.api.service.timeentry.session;

import com.telesoftas.jira2redmine.api.model.imports.ImportStatus;
import com.telesoftas.jira2redmine.api.model.imports.ImportStatusTableStatus;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.repository.ImportStatusRepository;
import com.telesoftas.jira2redmine.contract.FetchWorklogsError;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

@Log4j2
@Service
public class RedmineSessionTimeEntryPost implements SessionTimeEntryPost {

    private final ImportStatusRepository importStatusRepository;
    private final Executor executor;
    private final TimeEntryImporter importer;
    private final TimeEntryGetter timeEntryGetter;

    public RedmineSessionTimeEntryPost(ImportStatusRepository importStatusRepository,
                                       @Qualifier("taskExecutor") Executor executor,
                                       TimeEntryImporter importer,
                                       TimeEntryGetter timeEntryGetter) {
        this.importStatusRepository = importStatusRepository;
        this.executor = executor;
        this.importer = importer;
        this.timeEntryGetter = timeEntryGetter;
    }

    @Override
    public ImportStatus importTimeEntries(LocalDate from, LocalDate to) {
        ImportStatus importStatus = importStatusRepository.save(new ImportStatus());
        asyncImport(from, to, importStatus);
        return importStatus;
    }

    private void asyncImport(LocalDate from, LocalDate to, ImportStatus importStatus) {
        executor.execute(() -> {
            importStatusRepository.updateStatus(importStatus.getSessionID(), ImportStatusTableStatus.PENDING);
            try {
                importer.importTimeEntries(from, to, importStatus);
                importStatusRepository.updateStatus(importStatus.getSessionID(), ImportStatusTableStatus.IMPORTED);
            } catch (FetchWorklogsError e) {
                log.error(e);
                importStatusRepository.updateStatus(importStatus.getSessionID(), ImportStatusTableStatus.ERROR);
            }
        });
    }


    @Override
    public boolean checkIfImportIsComplete(ImportStatus importStatus) {
        Optional<ImportStatus> optionalImportStatus = importStatusRepository.findById(importStatus.getSessionID());
        return optionalImportStatus
                .filter(status ->
                        status.getStatus() == ImportStatusTableStatus.IMPORTED ||
                                status.getStatus() == ImportStatusTableStatus.ERROR)
                .isPresent();
    }

    @Override
    public List<TimeEntry> fetchCompletePost(ImportStatus importStatus) {
        return importStatusRepository.findById(importStatus.getSessionID()).stream()
                .filter(importStatus1 -> importStatus1.getStatus().equals(ImportStatusTableStatus.IMPORTED))
                .flatMap(__ -> timeEntryGetter.getTimeEntries(importStatus.getSessionID()).stream())
                .collect(Collectors.toList());
    }

    @Override
    public int importedWorklogCount(ImportStatus importStatus) {
        return timeEntryGetter.importedWorklogCount(importStatus);
    }
}
