package com.telesoftas.jira2redmine.api.model.redminedata.wrappers;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonRootName("time_entry")
public class PostTimeEntry {

    private Long projectId;
    /**
     * Redmine >= 4.1 only
     */
    private Long userId;
    private double hours;
    private String comments;
    private Long activityId;
    private LocalDate spentOn;

}
