package com.telesoftas.jira2redmine.api.model.redminedata.wrappers;

import com.telesoftas.jira2redmine.api.model.redminedata.Activity;
import lombok.Data;

import java.util.List;

@Data
public class ActivityData {

    private List<Activity> timeEntryActivities;

}
