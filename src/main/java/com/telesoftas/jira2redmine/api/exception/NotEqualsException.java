package com.telesoftas.jira2redmine.api.exception;

public class NotEqualsException extends Exception {

    public NotEqualsException() {
        super("Fields not equal");
    }

}
