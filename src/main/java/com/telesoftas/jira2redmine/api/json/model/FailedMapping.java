package com.telesoftas.jira2redmine.api.json.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FailedMapping {

    Object workLog;
    String exception;

}
