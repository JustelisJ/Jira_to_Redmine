package com.telesoftas.jira2redmine.api;

import com.telesoftas.jira2redmine.api.model.redminedata.Activity;
import com.telesoftas.jira2redmine.api.model.redminedata.Project;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.service.timeentry.TimeEntryService;
import com.telesoftas.jira2redmine.api.service.user.FindUserByJiraAccountId;
import com.telesoftas.jira2redmine.contract.FetchWorklogs;
import com.telesoftas.jira2redmine.contract.JiraWorklog;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.YearMonth;
import java.util.Optional;
import java.util.function.Function;

import static java.lang.String.format;

@Log4j2
@RequiredArgsConstructor
@Component
public class ImportTempoWorklogsOnBootup implements ApplicationRunner {

    public static final String TIGGER_FLAG_NAME = "import-tempo-worklogs";
    public static final String CDEV_PROJECT_KEY = "CDEV";
    public static final String CUBE_PROKECT_KEY = "CUBE";

    private final FindUserByJiraAccountId users;

    private final TimeEntryService timeEntries;

    private final FetchWorklogs tempo;

    private final ApplicationContext context;

    Clock clock = Clock.systemDefaultZone();

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (!args.containsOption(TIGGER_FLAG_NAME)) {
            return;
        }

        Project backendMasterProject = new Project(846L, "JAVA Master Project");
        Project stageTech = new Project(802L, "Stage Tech");

        tempo.fetchWorklogs(
                YearMonth.now(clock).atDay(1),
                YearMonth.now(clock).atEndOfMonth(),
                CDEV_PROJECT_KEY,
                CUBE_PROKECT_KEY
        ).forEach(worklog -> {
            if (CDEV_PROJECT_KEY.equals(worklog.getProjectKey())) {
                worklogToTimeEntry(worklog, backendMasterProject, this::worklogToActivity)
                        .ifPresent(timeEntries::postTimeEntry);
            }
            if (CUBE_PROKECT_KEY.equals(worklog.getProjectKey())) {
                worklogToTimeEntry(worklog, stageTech, this::developmentActivity)
                        .ifPresent(timeEntries::postTimeEntry);
            }
        });

        System.exit(SpringApplication.exit(context));
    }

    private Activity developmentActivity(JiraWorklog w) {
        return new Activity(9, "Development");
    }

    private Activity worklogToActivity(JiraWorklog w) {
        // todo include `_Activitygroup_` in JiraWorklog
        // todo write real converter for values
        /*
            Jira:
            "TSIdletime",
            "TSTravel",
            "TSRecruitmentactivities",
            "TSRepresentingTSexternally",
            "TSOn-boardingnewemployee",
            "TSBeing-onboarded",
            "TSConferencesTrainings",
            "TSSupportPlatformTTLonrequest",
            "TSCompanylevelactivities",
            "TS121withSTLHRTTL",
            "TS4hoursoff-projecttime"

            Redmine:
            135, 4-hours off-project time
            136, 121s with STL/HR/TTL
            137, Company level activities
            138, Suport Platform/TTL on request
            139, Conferences / Trainings
            140, Being on-boarded
            141, On-boarding a new employee
            142, Representing TS externally
            143, Recruitment activities
            144, Travel
            19,  Idle Time
         */

        // default if none is found
        return new Activity(135L, "4-hours off-project time ");
    }

    private Optional<TimeEntry> worklogToTimeEntry(JiraWorklog worklog, Project project, Function<JiraWorklog, Activity> activity) {
        return users.findUserByJiraAccountId(worklog.getAccountId())
                .map(user -> TimeEntry.builder()
                        .project(project)
                        .user(user)
                        .spentOn(worklog.getStartDate())
                        .hours(((double) worklog.getTimeSpentSeconds()) / 60.0 / 60.0)
                        .comments(format("%s: %s", worklog.getIssueID(), worklog.getDescription()))
                        .activity(activity.apply(worklog))
                        .build()
                );
    }
}
