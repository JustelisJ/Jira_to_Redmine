package com.telesoftas.jira2redmine.api.service.user;

import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;
import com.telesoftas.jira2redmine.api.model.redminedata.wrappers.UserData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class RedmineUserServiceTest {

    private static final String USER_FIRSTNAME = "Redmine";
    private static final String USER_LASTNAME = "Admin";
    private static final String USER_FULLNAME = "Redmine Admin";
    private static final String OTHER_USER_FULL_NAME = "Person Person";
    private static final long USER_ID = 1L;

    private static final RedmineUser user = RedmineUser.builder()
            .id(USER_ID)
            .firstname(USER_FIRSTNAME)
            .lastname(USER_LASTNAME)
            .build();

    @Mock
    RestTemplate restTemplate;

    UserService userService;

    UserData userData;
    ResponseEntity<UserData> responseEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        initializeUserData();
        initializeResponseEntity();
        whenThenForRestTemplate();
        userService = new RedmineUserService(restTemplate);
        ((RedmineUserService) userService).postConstruct();
    }

    private void whenThenForRestTemplate() {
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class),
                eq(UserData.class), anyMap()))
                .thenReturn(responseEntity);
    }

    private void initializeResponseEntity() {
        responseEntity = new ResponseEntity<>(userData, HttpStatus.ACCEPTED);
    }

    private void initializeUserData() {
        userData = new UserData();
        List<RedmineUser> users = new ArrayList<>();

        users.add(user);

        userData.setUsers(users);

        userData.setTotalCount(1);
    }

    @Test
    void findUserIdByName() {
        Optional<RedmineUser> id = userService.findUserByName(USER_FULLNAME);
        assertTrue(id.isPresent());
        assertEquals(user, id.get());
    }

    @Test
    void dontFindUserIdByName() {
        Optional<RedmineUser> id = userService.findUserByName(OTHER_USER_FULL_NAME);
        assertTrue(id.isEmpty());
    }
}
