package com.telesoftas.jira2redmine.api.service.user.mapping;

import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;
import com.telesoftas.jira2redmine.api.model.userMapping.JiraUser;
import com.telesoftas.jira2redmine.api.repository.UserRepository;
import com.telesoftas.jira2redmine.api.service.user.UserService;
import com.telesoftas.jira2redmine.contract.FetchWorklogs;
import com.telesoftas.jira2redmine.contract.FetchWorklogsError;
import com.telesoftas.jira2redmine.contract.JiraWorklog;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Service
public class UserMappingService implements UserMapping {

    private static final RedmineUser EMPTY_USER = RedmineUser.builder().build();
    private static final String CDEV_PROJECT_KEY = "CDEV";
    private static final String CUBE_PROJECT_KEY = "CUBE";

    private final FetchWorklogs fetchWorklogs;
    private final UserRepository userRepository;
    private final UserService userService;

    public UserMappingService(FetchWorklogs fetchWorklogs,
                              UserRepository userRepository,
                              UserService userService) {
        this.fetchWorklogs = fetchWorklogs;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @Override
    public Map<JiraUser, RedmineUser> getMappedUsers(Map<JiraUser, RedmineUser> jiraWorkLogs) {
        Map<JiraUser, RedmineUser> mapped = new HashMap<>();

        jiraWorkLogs.forEach((jiraUser, redmineUser) -> {
            if (redmineUser.getFirstname() != null && redmineUser.getLastname() != null && redmineUser.getLogin() != null) {
                mapped.put(jiraUser, redmineUser);
            }
        });

        return mapped;
    }

    @Override
    public Map<JiraUser, RedmineUser> getUnmappedUsers(Map<JiraUser, RedmineUser> jiraWorkLogs) {
        Map<JiraUser, RedmineUser> unmapped = new HashMap<>();

        jiraWorkLogs.forEach((jiraUser, redmineUser) -> {
            if (redmineUser.getFirstname() == null && redmineUser.getLastname() == null && redmineUser.getLogin() == null) {
                autoMap(jiraUser).ifPresentOrElse(
                        redmineUser1 -> unmapped.put(jiraUser, redmineUser1),
                        () -> unmapped.put(jiraUser, redmineUser));
            }
        });

        return unmapped;
    }

    @Override
    public Map<JiraUser, RedmineUser> getMappedUsers(LocalDate from, LocalDate to) {

        try {
            Collection<JiraWorklog> jiraWorklogs =
                    fetchWorklogs.fetchWorklogs(from, to, CDEV_PROJECT_KEY, CUBE_PROJECT_KEY);
            Map<JiraUser, RedmineUser> worklogUserMap = new HashMap<>();

            Map<Boolean, List<JiraWorklog>> databaseUserSplit = jiraWorklogs.stream()
                    .collect(Collectors.partitioningBy(jiraWorklog ->
                            userRepository.findById(jiraWorklog.getAccountId()).isPresent()));

            databaseUserSplit.get(false).forEach(jiraWorklog -> {
                log.warn(String.format("accountID %s not in DB", jiraWorklog.getAccountId()));
                worklogUserMap.putIfAbsent(new JiraUser(jiraWorklog.getDisplayName(), jiraWorklog.getAccountId()), EMPTY_USER);
            });
            databaseUserSplit.get(true).forEach(jiraWorklog ->
                    userRepository.findById(jiraWorklog.getAccountId())
                            .ifPresent(user -> userService.findUserById(user.getRedmineUserId())
                                    .ifPresentOrElse(
                                            redmineUser -> worklogUserMap.putIfAbsent(new JiraUser(jiraWorklog.getDisplayName(), jiraWorklog.getAccountId()), redmineUser),
                                            () -> log.error(String.format("User with ID %d is in the database, but not in redmine", user.getRedmineUserId())))));

            return worklogUserMap;
        } catch (FetchWorklogsError fetchWorklogsError) {
            log.error(fetchWorklogsError);
        }
        return new HashMap<>();
    }

    private Optional<RedmineUser> autoMap(JiraUser jiraUser) {
        return userService.findUserByName(jiraUser.getDisplayName());
    }
}
