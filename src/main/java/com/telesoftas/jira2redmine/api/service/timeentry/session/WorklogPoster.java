package com.telesoftas.jira2redmine.api.service.timeentry.session;

import com.telesoftas.jira2redmine.api.mapper.JiraPackageToTimeEntriesMapper;
import com.telesoftas.jira2redmine.api.model.imports.*;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.repository.ImportToWorklogRepository;
import com.telesoftas.jira2redmine.api.repository.WorklogRepository;
import com.telesoftas.jira2redmine.api.service.timeentry.TimeEntryService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class WorklogPoster {

    private final TimeEntryService timeEntryService;
    private final JiraPackageToTimeEntriesMapper mapper;
    private final WorklogRepository worklogRepository;
    private final ImportToWorklogRepository importToWorklogRepository;

    public WorklogPoster(TimeEntryService timeEntryService,
                         JiraPackageToTimeEntriesMapper mapper,
                         WorklogRepository worklogRepository,
                         ImportToWorklogRepository importToWorklogRepository) {
        this.timeEntryService = timeEntryService;
        this.mapper = mapper;
        this.worklogRepository = worklogRepository;
        this.importToWorklogRepository = importToWorklogRepository;
    }

    public void post(List<Worklog> worklogs, ImportStatus importStatus) {
        worklogs.forEach(worklog -> {
            ImportID importID = new ImportID(importStatus, worklog);
            // Create initial status
            createStatus(importID);
            mapper.mapToTimeEntry(worklog)
                    .flatMap(this::postTimeEntry)
                    .flatMap(entry -> linkTimeEntryWithWorkLog(worklog, entry))
                    .ifPresentOrElse(
                            __ -> updateStatus(importID, ImportToWorklogTableStatus.IMPORTED),
                            () -> updateStatus(importID, ImportToWorklogTableStatus.ERROR)
                    );
        });
    }

    private void createStatus(ImportID importID) {
        ImportToWorklog importToWorklog = new ImportToWorklog(importID);
        importToWorklogRepository.save(importToWorklog);
    }

    private Optional<TimeEntry> postTimeEntry(TimeEntry timeEntry) {
        return timeEntryService.postTimeEntry(timeEntry);
    }

    private Optional<Worklog> linkTimeEntryWithWorkLog(Worklog worklog, TimeEntry timeEntry) {
        worklog.setTimeEntryID(timeEntry.getId());
        return Optional.of(worklogRepository.save(worklog));
    }

    private void updateStatus(ImportID importID, ImportToWorklogTableStatus imported) {
        importToWorklogRepository.updateStatus(importID, imported);
    }

}
