package com.telesoftas.jira2redmine.api.service.user;

import com.telesoftas.jira2redmine.api.mapper.JiraUsersToRedmineUsersMapper;
import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;
import com.telesoftas.jira2redmine.integration.jiratempo.UsersIdToDisplayNameFetcher;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Service
@RequiredArgsConstructor
public class FindUserByJiraAccountId {

    private final JiraUsersToRedmineUsersMapper redmine;

    private final UsersIdToDisplayNameFetcher jira;

    private Map<String, RedmineUser> accountId2User;

    public synchronized Optional<RedmineUser> findUserByJiraAccountId(String accountId) {
        try {
            if (accountId2User == null) {
                accountId2User = redmine.mapJiraUsersToRedmineUsersId(jira.fetchAllUsersIdToDisplayName());
            }
            return ofNullable(accountId2User.get(accountId));
        } catch (IOException e) {
            return Optional.empty();
        }
    }
}
