package com.telesoftas.jira2redmine.api.model.redminedata;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Status {

    private Long id;
    private String name;

}
