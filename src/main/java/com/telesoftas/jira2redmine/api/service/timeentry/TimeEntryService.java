package com.telesoftas.jira2redmine.api.service.timeentry;

import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;

import java.util.List;
import java.util.Optional;

public interface TimeEntryService {

    Optional<TimeEntry> findTimeEntry(Long id);

    List<TimeEntry> getTimeEntries();

    Optional<TimeEntry> postTimeEntry(TimeEntry timeEntry);

    List<TimeEntry> postTimeEntries(List<TimeEntry> timeEntries);

    void updateTimeEntry(TimeEntry timeEntry, Long id);

}
