package com.telesoftas.jira2redmine.api.service.issue;

import com.telesoftas.jira2redmine.api.model.redminedata.Issue;

import java.util.Optional;

public interface IssueService {

    Optional<Issue> getIssueFromName(String name);

}
