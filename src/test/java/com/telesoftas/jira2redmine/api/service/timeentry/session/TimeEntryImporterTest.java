package com.telesoftas.jira2redmine.api.service.timeentry.session;

import com.telesoftas.jira2redmine.api.comparor.WorklogComparor;
import com.telesoftas.jira2redmine.api.model.imports.ImportStatus;
import com.telesoftas.jira2redmine.api.model.imports.ImportStatusTableStatus;
import com.telesoftas.jira2redmine.api.model.imports.Worklog;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.repository.WorklogRepository;
import com.telesoftas.jira2redmine.contract.FetchWorklogs;
import com.telesoftas.jira2redmine.contract.FetchWorklogsError;
import com.telesoftas.jira2redmine.contract.JiraWorklog;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class TimeEntryImporterTest {

    @Mock
    FetchWorklogs fetchWorklogs;
    @Mock
    WorklogRepository worklogRepository;
    @Mock
    WorklogPoster poster;
    @Mock
    WorklogUpdater updater;
    @Mock
    WorklogComparor comparor;

    TimeEntryImporter importer;

    LocalDate from;
    LocalDate to;
    ImportStatus importStatus;
    JiraWorklog jiraWorklog;
    Collection<JiraWorklog> jiraWorklogs;
    TimeEntry timeEntry;
    Worklog dbWorklog;
    List<Worklog> postList;
    List<Worklog> updateList;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        importer = new TimeEntryImporter(fetchWorklogs, worklogRepository, poster, updater, comparor);

        from = LocalDate.of(2020, 1, 1);
        to = LocalDate.of(2020, 1, 31);
        importStatus = new ImportStatus(1L, ImportStatusTableStatus.PENDING);
        jiraWorklog = JiraWorklog.builder()
                .worklogID(1L)
                .build();
        jiraWorklogs = new ArrayList<>();
        jiraWorklogs.add(jiraWorklog);
        timeEntry = TimeEntry.builder()
                .id(2L)
                .build();
        dbWorklog = Worklog.builder()
                .worklogID(1L)
                .timeEntryID(2L)
                .build();
        postList = new ArrayList<>();
        updateList = new ArrayList<>();
        postList.add(Worklog.builder().worklogID(1L).build());
        updateList.add(Worklog.builder().worklogID(1L).timeEntryID(2L).build());
    }

    @Test
    void importTimeEntries_throwsFetchWorklogsError() throws FetchWorklogsError {
        when(fetchWorklogs.fetchWorklogs(any(LocalDate.class), any(LocalDate.class), anyString(), anyString()))
                .thenThrow(FetchWorklogsError.class);

        assertThrows(FetchWorklogsError.class, () -> {
            importer.importTimeEntries(from, to, importStatus);
        });
        verify(poster, times(0)).post(anyList(), any(ImportStatus.class));
        verify(updater, times(0)).update(anyList(), any(ImportStatus.class));
    }

    @Test
    void importTimeEntries_CantFindWorklogsInRepository() throws FetchWorklogsError {
        when(fetchWorklogs.fetchWorklogs(any(LocalDate.class), any(LocalDate.class), anyString(), anyString()))
                .thenReturn(jiraWorklogs);
        when(worklogRepository.findWorklogByWorklogID(anyLong())).thenReturn(Optional.empty());

        importer.importTimeEntries(from, to, importStatus);

        verify(poster, times(1)).post(eq(postList), any(ImportStatus.class));
        verify(updater, times(1)).update(eq(new ArrayList<>()), any(ImportStatus.class));
    }

    @Test
    void importTimeEntries_FindsWorklogInDBAndWorklogsAreTheSame() throws FetchWorklogsError {
        when(fetchWorklogs.fetchWorklogs(any(LocalDate.class), any(LocalDate.class), anyString(), anyString()))
                .thenReturn(jiraWorklogs);
        when(worklogRepository.findWorklogByWorklogID(anyLong())).thenReturn(Optional.of(dbWorklog));
        when(comparor.worklogsEqual(any(Worklog.class), any(JiraWorklog.class))).thenReturn(true);

        importer.importTimeEntries(from, to, importStatus);

        verify(poster, times(1)).post(eq(new ArrayList<>()), any(ImportStatus.class));
        verify(updater, times(1)).update(eq(new ArrayList<>()), any(ImportStatus.class));
    }

    @Test
    void importTimeEntries_FindsWorklogInDBAndWorklogsAreDifferent() throws FetchWorklogsError {
        when(fetchWorklogs.fetchWorklogs(any(LocalDate.class), any(LocalDate.class), anyString(), anyString()))
                .thenReturn(jiraWorklogs);
        when(worklogRepository.findWorklogByWorklogID(anyLong())).thenReturn(Optional.of(dbWorklog));
        when(comparor.worklogsEqual(any(Worklog.class), any(JiraWorklog.class))).thenReturn(false);

        importer.importTimeEntries(from, to, importStatus);

        verify(poster, times(1)).post(eq(new ArrayList<>()), any(ImportStatus.class));
        verify(updater, times(1)).update(eq(updateList), any(ImportStatus.class));
    }
}