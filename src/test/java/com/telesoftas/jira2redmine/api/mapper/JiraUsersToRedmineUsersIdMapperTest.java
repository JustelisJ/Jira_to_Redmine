package com.telesoftas.jira2redmine.api.mapper;

import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;
import com.telesoftas.jira2redmine.api.service.user.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class JiraUsersToRedmineUsersIdMapperTest {

    @Mock
    UserService userService;

    JiraUsersToRedmineUsersMapper mapper;

    RedmineUser user;
    Map<String, String> map;

    void initializeData() {
        user = RedmineUser.builder()
                .id(1L)
                .firstname("person")
                .lastname("person")
                .build();

        map = new HashMap<>();
        map.put("123456", "person person");
    }

    @BeforeEach
    void setUp() {
        initializeData();
        MockitoAnnotations.initMocks(this);
        mapper = new JiraUsersToRedmineUsersMapper(userService);
    }

    @Test
    void mapJiraUsersToRedmineUsersId() {
        when(userService.findUserByName("person person")).thenReturn(Optional.of(user));

        Map<String, RedmineUser> mappedUserIds = mapper.mapJiraUsersToRedmineUsersId(map);

        assertEquals(user, mappedUserIds.get("123456"));
    }
}