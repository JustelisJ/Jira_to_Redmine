package com.telesoftas.jira2redmine.api.comparor;

import com.telesoftas.jira2redmine.api.exception.NotEqualsException;
import com.telesoftas.jira2redmine.api.model.imports.Worklog;
import com.telesoftas.jira2redmine.contract.JiraWorklog;
import org.springframework.stereotype.Component;

@Component
public class WorklogComparor {

    public boolean worklogsEqual(Worklog dbWorklog, JiraWorklog jiraWorklog) {
        try {
            checkIfEquals(dbWorklog.getWorklogID(), jiraWorklog.getWorklogID());
            checkIfEquals(dbWorklog.getAccountId(), jiraWorklog.getAccountId());
            checkIfEquals(dbWorklog.getActivity(), jiraWorklog.getActivity());
            checkIfEquals(dbWorklog.getDescription(), jiraWorklog.getDescription());
            checkIfEquals(dbWorklog.getIssueID(), jiraWorklog.getIssueID());
            checkIfEquals(dbWorklog.getProjectKey(), jiraWorklog.getProjectKey());
            checkIfEquals(dbWorklog.getStartDate(), jiraWorklog.getStartDate());
            checkIfEquals(dbWorklog.getTimeSpentSeconds(), jiraWorklog.getTimeSpentSeconds());
            return true;
        } catch (NotEqualsException e) {
            return false;
        }
    }

    private <T> void checkIfEquals(T field1, T field2) throws NotEqualsException {
        if (field1 != null ? !field1.equals(field2) : field2 != null)
            throw new NotEqualsException();
    }

}
