package com.telesoftas.jira2redmine.api.service.timeentry.session;

import com.telesoftas.jira2redmine.api.comparor.WorklogComparor;
import com.telesoftas.jira2redmine.api.model.imports.ImportStatus;
import com.telesoftas.jira2redmine.api.model.imports.Worklog;
import com.telesoftas.jira2redmine.api.repository.WorklogRepository;
import com.telesoftas.jira2redmine.contract.FetchWorklogs;
import com.telesoftas.jira2redmine.contract.FetchWorklogsError;
import com.telesoftas.jira2redmine.contract.JiraWorklog;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.partitioningBy;

@Component
@Log4j2
public class TimeEntryImporter {

    private static final String CDEV_PROJECT_KEY = "CDEV";
    private static final String CUBE_PROJECT_KEY = "CUBE";

    private final FetchWorklogs fetchWorklogs;
    private final WorklogRepository worklogRepository;
    private final WorklogPoster post;
    private final WorklogUpdater update;
    private final WorklogComparor worklogComparor;

    public TimeEntryImporter(FetchWorklogs fetchWorklogs,
                             WorklogRepository worklogRepository,
                             WorklogPoster post,
                             WorklogUpdater update,
                             WorklogComparor worklogComparor) {
        this.fetchWorklogs = fetchWorklogs;
        this.worklogRepository = worklogRepository;
        this.post = post;
        this.update = update;
        this.worklogComparor = worklogComparor;
    }

    public void importTimeEntries(LocalDate from, LocalDate to, ImportStatus importStatus) throws FetchWorklogsError {
        Map<Boolean, List<Worklog>> worklogs =
                fetchWorklogs.fetchWorklogs(from, to, CDEV_PROJECT_KEY, CUBE_PROJECT_KEY).stream()
                        .flatMap(this::convertToNewOrUpdatableWorklog)
                        .collect(partitioningBy(Worklog::isNew));

        post.post(worklogs.get(true), importStatus);
        update.update(worklogs.get(false), importStatus);

    }

    private Stream<Worklog> convertToNewOrUpdatableWorklog(JiraWorklog worklog) {
        return worklogRepository.findWorklogByWorklogID(worklog.getWorklogID())
                .map(dbWorklog -> {
                    if (worklogComparor.worklogsEqual(dbWorklog, worklog)) {
                        return Stream.<Worklog>empty();
                    }
                    return Stream.of(new Worklog(worklog, dbWorklog.getTimeEntryID()));
                })
                .orElseGet(() -> Stream.of(new Worklog(worklog)));
    }

}
