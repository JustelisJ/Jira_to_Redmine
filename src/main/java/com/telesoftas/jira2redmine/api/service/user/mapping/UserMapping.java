package com.telesoftas.jira2redmine.api.service.user.mapping;

import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;
import com.telesoftas.jira2redmine.api.model.userMapping.JiraUser;

import java.time.LocalDate;
import java.util.Map;

public interface UserMapping {

    Map<JiraUser, RedmineUser> getMappedUsers(Map<JiraUser, RedmineUser> jiraWorkLogs);

    Map<JiraUser, RedmineUser> getUnmappedUsers(Map<JiraUser, RedmineUser> jiraWorkLogs);

    Map<JiraUser, RedmineUser> getMappedUsers(LocalDate from, LocalDate to);

}
