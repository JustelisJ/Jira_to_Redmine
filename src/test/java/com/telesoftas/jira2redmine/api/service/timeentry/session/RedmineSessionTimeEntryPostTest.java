package com.telesoftas.jira2redmine.api.service.timeentry.session;

import com.telesoftas.jira2redmine.api.model.imports.ImportStatus;
import com.telesoftas.jira2redmine.api.model.imports.ImportStatusTableStatus;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.repository.ImportStatusRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class RedmineSessionTimeEntryPostTest {

    @Mock
    ImportStatusRepository importStatusRepository;
    @Mock
    Executor executor;
    @Mock
    TimeEntryImporter importer;
    @Mock
    TimeEntryGetter getter;

    RedmineSessionTimeEntryPost post;

    ImportStatus completeImpostStatus;
    ImportStatus inProgressImpostStatus;
    ImportStatus errorImportStatus;
    List<TimeEntry> getterList;
    LocalDate from;
    LocalDate to;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        post = new RedmineSessionTimeEntryPost(importStatusRepository, executor, importer, getter);

        completeImpostStatus = new ImportStatus(1L, ImportStatusTableStatus.IMPORTED);
        inProgressImpostStatus = new ImportStatus(1L, ImportStatusTableStatus.PENDING);
        errorImportStatus = new ImportStatus(1L, ImportStatusTableStatus.ERROR);
        getterList = new ArrayList<>();
        getterList.add(TimeEntry.builder().id(1L).build());
        from = LocalDate.of(2020, 1, 1);
        to = LocalDate.of(2020, 1, 31);
    }

    @Test
    void importTimeEntries() {
        when(importStatusRepository.save(any(ImportStatus.class))).thenReturn(inProgressImpostStatus);

        ImportStatus returnedImportStatus = post.importTimeEntries(from, to);

        assertEquals(inProgressImpostStatus, returnedImportStatus);
    }

    @Test
    void checkIfImportIsComplete_PendingImportStatus() {
        when(importStatusRepository.findById(anyLong())).thenReturn(Optional.of(inProgressImpostStatus));

        boolean result = post.checkIfImportIsComplete(inProgressImpostStatus);

        assertEquals(false, result);
    }

    @Test
    void checkIfImportIsComplete_CompleteImportStatus() {
        when(importStatusRepository.findById(anyLong())).thenReturn(Optional.of(completeImpostStatus));

        boolean result = post.checkIfImportIsComplete(completeImpostStatus);

        assertEquals(true, result);
    }

    @Test
    void checkIfImportIsComplete_ErrorImportStatus() {
        when(importStatusRepository.findById(anyLong())).thenReturn(Optional.of(errorImportStatus));

        boolean result = post.checkIfImportIsComplete(errorImportStatus);

        assertEquals(true, result);
    }

    @Test
    void fetchCompletePost_ImportStatusIncomplete() {
        when(importStatusRepository.findById(anyLong())).thenReturn(Optional.of(inProgressImpostStatus));

        List<TimeEntry> fetchedTimeEntries = post.fetchCompletePost(inProgressImpostStatus);

        assertEquals(new ArrayList<>(), fetchedTimeEntries);
    }

    @Test
    void fetchCompletePost_ImportStatusComplete() {
        when(importStatusRepository.findById(anyLong())).thenReturn(Optional.of(completeImpostStatus));
        when(getter.getTimeEntries(anyLong())).thenReturn(getterList);

        List<TimeEntry> fetchedTimeEntries = post.fetchCompletePost(inProgressImpostStatus);

        assertEquals(getterList, fetchedTimeEntries);
    }

    @Test
    void importedWorklogCount() {
        when(getter.importedWorklogCount(any(ImportStatus.class))).thenReturn(1);

        int count = post.importedWorklogCount(completeImpostStatus);

        assertEquals(1, count);
    }
}