package com.telesoftas.jira2redmine.api.service.user;

import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;

import java.util.Collection;
import java.util.Optional;

public interface UserService {

    Optional<RedmineUser> findUserByName(String fullName);

    Optional<RedmineUser> findUserById(Long id);

    Collection<RedmineUser> getUserList();

}
