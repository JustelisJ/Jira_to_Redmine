package com.telesoftas.jira2redmine.api.repository;

import com.telesoftas.jira2redmine.api.model.userMapping.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, String> {

    List<User> findAll();

}
