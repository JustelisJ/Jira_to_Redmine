package com.telesoftas.jira2redmine.integration.jiratempo;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UsersIdToDisplayNameFetcher implements UsersIdToDisplayNameFetcherInterface {

    private final JiraUserEmailFetcher fetchJiraUserEmail;
    private final ObjectMapper mapper;


    public UsersIdToDisplayNameFetcher(JiraUserEmailFetcher fetchJiraUserEmail, ObjectMapper mapper) {
        this.fetchJiraUserEmail = fetchJiraUserEmail;
        this.mapper = mapper;

    }

    public Map<String, String> fetchAllUsersIdToDisplayName() throws IOException {
        int startAt = 0;
        final int maxResults = 50;

        final Map<String, String> map = new HashMap<>();

        JsonNode actualObj;
        do {
            String response = fetchJiraUserEmail.getResponse(startAt, maxResults);

            actualObj = mapper.readTree(response);
            for (JsonNode node : actualObj) {
                JsonNode accountIdNode = node.get("accountId");
                JsonNode displayNameNode = node.get("displayName");

                if (accountIdNode != null && displayNameNode != null) {
                    String accountId = accountIdNode.asText();
                    String displayName = displayNameNode.asText();
                    map.put(accountId, displayName);
                }
            }

            startAt += maxResults;
        }
        while (!actualObj.isEmpty());

        return map;

    }


}
