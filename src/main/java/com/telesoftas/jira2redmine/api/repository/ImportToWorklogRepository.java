package com.telesoftas.jira2redmine.api.repository;

import com.telesoftas.jira2redmine.api.model.imports.ImportID;
import com.telesoftas.jira2redmine.api.model.imports.ImportToWorklog;
import com.telesoftas.jira2redmine.api.model.imports.ImportToWorklogTableStatus;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

public interface ImportToWorklogRepository extends CrudRepository<ImportToWorklog, ImportID> {

    Set<ImportToWorklog> findAllByCompositeKey_ImportStatus_SessionIDAndImportStatus(Long sessionID,
                                                                                     ImportToWorklogTableStatus status);

    int countImportToWorklogByCompositeKey_ImportStatus_SessionID(Long sessionID);

    @Transactional
    @Modifying
    @Query("update ImportToWorklog u set u.importStatus = :status where u.compositeKey = :importID")
    void updateStatus(@Param("importID") ImportID importID, @Param("status") ImportToWorklogTableStatus status);

}
