package com.telesoftas.jira2redmine.api.service.timeentry.session;

import com.telesoftas.jira2redmine.api.model.imports.*;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.repository.ImportToWorklogRepository;
import com.telesoftas.jira2redmine.api.repository.WorklogRepository;
import com.telesoftas.jira2redmine.api.service.timeentry.TimeEntryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class TimeEntryGetterTest {

    @Mock
    ImportToWorklogRepository importToWorklogRepository;
    @Mock
    TimeEntryService timeEntryService;
    @Mock
    WorklogRepository worklogRepository;

    TimeEntryGetter getter;

    TimeEntry timeEntry;
    List<TimeEntry> timeEntries;
    List<TimeEntry> emptyTimeEntryList;
    ImportStatus importStatus;
    ImportToWorklog importToWorklog;
    Set<ImportToWorklog> importToWorklogs;
    Worklog worklog;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        getter = new TimeEntryGetter(importToWorklogRepository, timeEntryService, worklogRepository);

        timeEntry = TimeEntry.builder()
                .id(1L)
                .build();
        timeEntries = new ArrayList<>();
        timeEntries.add(timeEntry);
        emptyTimeEntryList = new ArrayList<>();
        importStatus = new ImportStatus(1L, ImportStatusTableStatus.IMPORTED);
        worklog = Worklog.builder()
                .worklogID(1L)
                .timeEntryID(1L)
                .build();
        importToWorklog = ImportToWorklog.builder()
                .compositeKey(new ImportID(importStatus, worklog))
                .build();
        importToWorklogs = new HashSet<>();
        importToWorklogs.add(importToWorklog);
    }

    @Test
    void getTimeEntries_ImportToWorklogRepositoryNoEntryWithThatID() {
        when(importToWorklogRepository.findAllByCompositeKey_ImportStatus_SessionIDAndImportStatus(anyLong(),
                any(ImportToWorklogTableStatus.class))).thenReturn(Set.of());

        List<TimeEntry> result = getter.getTimeEntries(1L);

        assertEquals(emptyTimeEntryList, result);
    }

    @Test
    void getTimeEntries_WorklogRepositoryMissingEntry() {
        when(importToWorklogRepository.findAllByCompositeKey_ImportStatus_SessionIDAndImportStatus(anyLong(),
                any(ImportToWorklogTableStatus.class))).thenReturn(importToWorklogs);
        when(worklogRepository.findWorklogByWorklogID(anyLong())).thenReturn(Optional.empty());

        List<TimeEntry> result = getter.getTimeEntries(1L);

        assertEquals(emptyTimeEntryList, result);
    }

    @Test
    void getTimeEntries_TimeEntryServiceCantFindTimeEntry() {
        when(importToWorklogRepository.findAllByCompositeKey_ImportStatus_SessionIDAndImportStatus(anyLong(),
                any(ImportToWorklogTableStatus.class))).thenReturn(importToWorklogs);
        when(worklogRepository.findWorklogByWorklogID(anyLong())).thenReturn(Optional.of(worklog));
        when(timeEntryService.findTimeEntry(anyLong())).thenReturn(Optional.empty());

        List<TimeEntry> result = getter.getTimeEntries(1L);

        assertEquals(emptyTimeEntryList, result);
    }

    @Test
    void getTimeEntries_successfullyGetTimeEntries() {
        when(importToWorklogRepository.findAllByCompositeKey_ImportStatus_SessionIDAndImportStatus(anyLong(),
                any(ImportToWorklogTableStatus.class))).thenReturn(importToWorklogs);
        when(worklogRepository.findWorklogByWorklogID(anyLong())).thenReturn(Optional.of(worklog));
        when(timeEntryService.findTimeEntry(anyLong())).thenReturn(Optional.of(timeEntry));

        List<TimeEntry> result = getter.getTimeEntries(1L);

        assertEquals(timeEntries, result);
    }

    @Test
    void importedWorklogCount() {
        when(importToWorklogRepository.countImportToWorklogByCompositeKey_ImportStatus_SessionID(anyLong()))
                .thenReturn(1);

        int count = getter.importedWorklogCount(importStatus);

        assertEquals(1, count);
    }
}