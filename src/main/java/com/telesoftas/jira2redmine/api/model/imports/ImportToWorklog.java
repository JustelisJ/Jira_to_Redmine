package com.telesoftas.jira2redmine.api.model.imports;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Builder
public class ImportToWorklog {

    @EmbeddedId
    private ImportID compositeKey;

    private ImportToWorklogTableStatus importStatus;
    private LocalDate createdAt;
    private LocalDate updatedAt;

    public ImportToWorklog(ImportID compositeKey) {
        this.compositeKey = compositeKey;
        importStatus = ImportToWorklogTableStatus.CREATED;
        createdAt = LocalDate.now();
        updatedAt = LocalDate.now();
    }

}
