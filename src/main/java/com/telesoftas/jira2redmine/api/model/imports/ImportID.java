package com.telesoftas.jira2redmine.api.model.imports;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.OneToOne;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Embeddable
public class ImportID implements Serializable {

    @OneToOne
    private ImportStatus importStatus;
    @OneToOne
    private Worklog worklog;

}
