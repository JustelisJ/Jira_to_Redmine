package com.telesoftas.jira2redmine.api.service.timeentry;

import com.telesoftas.jira2redmine.api.json.model.FailedPost;
import com.telesoftas.jira2redmine.api.json.writter.FailedPostWriterToFile;
import com.telesoftas.jira2redmine.api.mapper.TimeEntryToPostTimeEntryMapper;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.model.redminedata.wrappers.PostTimeEntry;
import com.telesoftas.jira2redmine.api.model.redminedata.wrappers.SingleTimeEntryData;
import com.telesoftas.jira2redmine.api.model.redminedata.wrappers.TimeEntryData;
import com.telesoftas.jira2redmine.api.model.redminedata.wrappers.TimeEntryPayload;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.*;

@Log4j2
@Service
public class RedmineTimeEntryService implements TimeEntryService {

    public static final String FAILED_POSTING_MESSAGE = "Couldn't post time entry from Service";
    public static final String REDMINE_SWITCH_USER_HEADER = "X-Redmine-Switch-User";
    public static final String TIME_ENTRY_SERVICE_ERROR = "Couldn't fetch time entries from Time Entry service";
    private final String url;

    private final RestTemplate restTemplate;
    private final FailedPostWriterToFile writter;

    public RedmineTimeEntryService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.url = "/time_entries.json";
        writter = new FailedPostWriterToFile();
    }

    @Override
    public Optional<TimeEntry> findTimeEntry(Long id) {
        String getURL = String.format("/time_entries/%d.json", id);

        ResponseEntity<SingleTimeEntryData> responseData = restTemplate
                .getForEntity(getURL, SingleTimeEntryData.class);

        return Optional.ofNullable(responseData.getBody().getTime_entry());
    }

    @Override
    public List<TimeEntry> getTimeEntries() {
        List<TimeEntry> userSet = new ArrayList<>();
        String getUrl = url + "?limit={limit}&offset={offset}";

        int totalCount = 0;
        int offset = 0;
        int limit = 100;

        ResponseEntity<TimeEntryData> responseData;
        do {
            Map<String, String> params = new HashMap<>();
            params.put("limit", String.valueOf(limit));
            params.put("offset", String.valueOf(offset));

            responseData = restTemplate.getForEntity(getUrl, TimeEntryData.class);
            Optional<TimeEntryData> returnedValue = Optional.ofNullable(responseData.getBody());
            if (returnedValue.isPresent()) {
                userSet.addAll(returnedValue.get().getTimeEntries());
            } else {
                log.error(TIME_ENTRY_SERVICE_ERROR);
                break;
            }
            if (totalCount == 0) {
                totalCount = returnedValue.get().getTotalCount();
            }
            offset += returnedValue.get().getTimeEntries().size();
        } while (totalCount != offset);
        return userSet;
    }

    /**
     * TO POST TIME FOR OTHER USER
     * on Redmine < 4.1 use `X-Redmine-Switch-RedmineUser: <login>` header (requires admin role, impersonation feature)
     * on Redmine >= 4.1 `user_id` field is available (requires "Log spent time for other users" permission)
     */
    @Override
    public Optional<TimeEntry> postTimeEntry(TimeEntry timeEntry) {
        PostTimeEntry postTimeEntry = TimeEntryToPostTimeEntryMapper.mapToPostTimeEntry(timeEntry);
        HttpHeaders headers = new HttpHeaders();
        headers.set(REDMINE_SWITCH_USER_HEADER, timeEntry.getUser().getLogin());
        URI expandedUrl = restTemplate.getUriTemplateHandler().expand(url);
        try {
            ResponseEntity<TimeEntryPayload<TimeEntry>> responseEntity = restTemplate.exchange(
                    new RequestEntity<>(postTimeEntry, headers, HttpMethod.POST, expandedUrl),
                    new ReadTimeEntryPayloadReference()
            );
            Optional<TimeEntryPayload<TimeEntry>> returnOptional = Optional.ofNullable(responseEntity.getBody());
            if (returnOptional.isPresent()) {
                log.info("Posted time entry");
                return returnOptional.map(TimeEntryPayload::getTimeEntry);
            } else {
                log.error(FAILED_POSTING_MESSAGE);
                return Optional.empty();
            }
        } catch (Exception e) {
            log.error("Error while posting", e);
            return Optional.empty();
        }
    }

    @Override
    public List<TimeEntry> postTimeEntries(List<TimeEntry> timeEntries) {
        int count = 0;
        ArrayList<TimeEntry> postedEntries = new ArrayList<>();
        ArrayList<FailedPost> failedPosts = new ArrayList<>();
        for (TimeEntry timeEntry : timeEntries) {
            Optional<TimeEntry> returnTimeEntry = postTimeEntry(timeEntry);
            if (returnTimeEntry.isPresent()) {
                postedEntries.add(returnTimeEntry.get());
                count++;
            } else {
                log.error(FAILED_POSTING_MESSAGE);
                failedPosts.add(new FailedPost(timeEntry, FAILED_POSTING_MESSAGE));
            }
        }
        if (!failedPosts.isEmpty()) {
            writter.logWrongObjectToFile(failedPosts);
        }
        log.info("Posted " + count + " work logs, out of " + timeEntries.size());
        return postedEntries;
    }

    @Override
    public void updateTimeEntry(TimeEntry timeEntry, Long timeEntryID) {
        String putURL = String.format("/time_entries/%d.json", timeEntryID);
        PostTimeEntry postTimeEntry = TimeEntryToPostTimeEntryMapper.mapToPostTimeEntry(timeEntry);
        HttpHeaders headers = new HttpHeaders();
        headers.set(REDMINE_SWITCH_USER_HEADER, timeEntry.getUser().getLogin());
        URI expandedUrl = restTemplate.getUriTemplateHandler().expand(putURL);
        try {
            restTemplate.put(expandedUrl, postTimeEntry);
        } catch (Exception e) {
            log.error("Error while updating", e);
        }
    }

    private static class ReadTimeEntryPayloadReference extends ParameterizedTypeReference<TimeEntryPayload<TimeEntry>> {

    }
}
