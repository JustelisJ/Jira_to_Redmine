package com.telesoftas.jira2redmine.api.model.userMapping;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JiraUser {

    String displayName;
    String accountId;

}
