package com.telesoftas.jira2redmine.contract;

import java.time.LocalDate;
import java.util.Collection;

public interface FetchWorklogs {
    Collection<JiraWorklog> fetchWorklogs(LocalDate from, LocalDate to, String... projectKeys)
            throws FetchWorklogsError;
}
