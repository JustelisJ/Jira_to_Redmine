package com.telesoftas.jira2redmine.api.model.redminedata.wrappers;

import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SingleTimeEntryData {

    private TimeEntry time_entry;

}
