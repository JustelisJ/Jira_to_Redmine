package com.telesoftas.jira2redmine.api.service.timeentry.session;

import com.telesoftas.jira2redmine.api.model.imports.ImportStatus;
import com.telesoftas.jira2redmine.api.model.imports.ImportToWorklog;
import com.telesoftas.jira2redmine.api.model.imports.ImportToWorklogTableStatus;
import com.telesoftas.jira2redmine.api.model.imports.Worklog;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.repository.ImportToWorklogRepository;
import com.telesoftas.jira2redmine.api.repository.WorklogRepository;
import com.telesoftas.jira2redmine.api.service.timeentry.TimeEntryService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class TimeEntryGetter {

    private final ImportToWorklogRepository importToWorklogRepository;
    private final TimeEntryService timeEntryService;
    private final WorklogRepository worklogRepository;

    public TimeEntryGetter(ImportToWorklogRepository importToWorklogRepository,
                           TimeEntryService timeEntryService,
                           WorklogRepository worklogRepository) {
        this.importToWorklogRepository = importToWorklogRepository;
        this.timeEntryService = timeEntryService;
        this.worklogRepository = worklogRepository;
    }

    public List<TimeEntry> getTimeEntries(Long sessionID) {
        return importToWorklogRepository
                .findAllByCompositeKey_ImportStatus_SessionIDAndImportStatus(sessionID,
                        ImportToWorklogTableStatus.IMPORTED).stream()
                .flatMap(this::getWorklogStream)
                .flatMap(this::getTimeEntryStream)
                .collect(Collectors.toList());
    }

    private Stream<Worklog> getWorklogStream(ImportToWorklog importToWorklog) {
        Long worklogID = importToWorklog.getCompositeKey().getWorklog().getWorklogID();
        return worklogRepository.findWorklogByWorklogID(worklogID).stream();
    }

    private Stream<TimeEntry> getTimeEntryStream(Worklog worklog) {
        Long timeEntryID = worklog.getTimeEntryID();
        return timeEntryService.findTimeEntry(timeEntryID).stream();
    }

    public int importedWorklogCount(ImportStatus importStatus) {
        return importToWorklogRepository
                .countImportToWorklogByCompositeKey_ImportStatus_SessionID(importStatus.getSessionID());
    }

}
