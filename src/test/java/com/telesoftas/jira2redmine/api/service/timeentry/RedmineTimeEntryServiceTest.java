package com.telesoftas.jira2redmine.api.service.timeentry;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telesoftas.jira2redmine.api.config.JacksonConfig;
import com.telesoftas.jira2redmine.api.config.RestTemplateConfig;
import com.telesoftas.jira2redmine.api.model.redminedata.Activity;
import com.telesoftas.jira2redmine.api.model.redminedata.Issue;
import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.test.web.client.MockRestServiceServer;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.ExpectedCount.times;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RestClientTest({RedmineTimeEntryService.class, JacksonConfig.class, RestTemplateConfig.class})
class RedmineTimeEntryServiceTest {

    public static final long TIME_ENTRY_ID = 123456L;
    private static final long ACTIVITY_ID = 9L;
    private static final long PROJECT_ID = 123L;
    private static final String OTHER_USER_LOGIN = "nermnv";

    @Autowired
    RedmineTimeEntryService entryService;

    @Autowired
    MockRestServiceServer redmine;

    ObjectMapper json = new ObjectMapper();

    @Test
    void getTimeEntries() {
        this.redmine.expect(requestTo("/time_entries.json?limit=100&offset=0"))
                .andExpect(method(GET))
                .andRespond(withSuccess(makeListOfTimeEntriesResponse(), APPLICATION_JSON));

        List<TimeEntry> returnedData = entryService.getTimeEntries();

        assertEquals(1, returnedData.size());
        assertEquals(TIME_ENTRY_ID, returnedData.get(0).getId());
    }

    @Test
    void postTimeEntry() {
        this.redmine.expect(once(), requestTo("/time_entries.json"))
                .andExpect(method(POST))
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(makeCreateTimeEntryRequestJson()))
                .andRespond(withSuccess(makeSingleTimeEntryResponse(), APPLICATION_JSON));

        Optional<TimeEntry> returnedPost = entryService.postTimeEntry(makeTimeEntry());

        assertTrue(returnedPost.isPresent());
        assertEquals(TIME_ENTRY_ID, returnedPost.get().getId());
    }

    @Test
    void postTimeEntryForOtherUserWithHeader() {
        this.redmine.expect(requestTo("/time_entries.json"))
                .andExpect(header("X-Redmine-Switch-RedmineUser", OTHER_USER_LOGIN))
                .andRespond(withSuccess(makeSingleTimeEntryResponse(), APPLICATION_JSON));

        entryService.postTimeEntry(makeTimeEntry());
    }

    @Test
    void postTimeEntries() {
        this.redmine.expect(times(2), requestTo("/time_entries.json"))
                .andExpect(method(POST))
                .andRespond(withSuccess(makeSingleTimeEntryResponse(), APPLICATION_JSON));
        List<TimeEntry> returnedPosts =
                entryService.postTimeEntries(List.of(makeTimeEntry(), makeTimeEntry()));

        assertFalse(returnedPosts.isEmpty());
        assertEquals(TIME_ENTRY_ID, returnedPosts.get(0).getId());
        assertEquals(TIME_ENTRY_ID, returnedPosts.get(1).getId());
    }

    private String makeCreateTimeEntryRequestJson() {
        return json.createObjectNode()
                .set(
                        "time_entry",
                        json.createObjectNode()
//                    .put("project_id", ?)
                                .put("issue_id", 55399)
                                .put("spent_on", "2020-11-02")
                                .put("hours", 1.0)
                                .put("activity_id", ACTIVITY_ID)
                                .put("comments", "short description")
                ).toPrettyString();
    }

    private String makeSingleTimeEntryResponse() {
        return "{\n"
                + "    \"time_entry\": {\n"
                + "        \"id\": " + TIME_ENTRY_ID + ",\n"
                + "        \"project\": {\n"
                + "            \"id\": 132,\n"
                + "            \"name\": \"Test project\"\n"
                + "        },\n"
                + "        \"issue\": {\n"
                + "            \"id\": 55399\n"
                + "        },\n"
                + "        \"user\": {\n"
                + "            \"id\": 234,\n"
                + "            \"name\": \"Vardenis Pavardenis\"\n"
                + "        },\n"
                + "        \"activity\": {\n"
                + "            \"id\": " + ACTIVITY_ID + ",\n"
                + "            \"name\": \"Development\"\n"
                + "        },\n"
                + "        \"hours\": 1.0,\n"
                + "        \"comments\": \"short description\",\n"
                + "        \"spent_on\": \"2020-11-02\",\n"
                + "        \"created_on\": \"2020-11-01T12:54:00Z\",\n"
                + "        \"updated_on\": \"2020-11-01T12:54:00Z\"\n"
                + "    }\n"
                + "}";
    }

    private TimeEntry makeTimeEntry() {
        var timeEntry = new TimeEntry();
        timeEntry.setComments("short description");
        timeEntry.setHours(1.0);
        timeEntry.setSpentOn(LocalDate.parse("2020-11-02"));

        Issue issue = Issue.builder().id(55399L).build();
        timeEntry.setIssue(issue);

        RedmineUser user = RedmineUser.builder().id(234L).login(OTHER_USER_LOGIN).build();
        timeEntry.setUser(user);

        Activity activity = new Activity();
        activity.setId(9L);
        timeEntry.setActivity(activity);

        return timeEntry;
    }

    private String makeListOfTimeEntriesResponse() {
        return "{\n"
                + "  \"time_entries\": [\n"
                + "    {\n"
                + "      \"id\": " + TIME_ENTRY_ID + ",\n"
                + "      \"project\": {\n"
                + "        \"id\": " + PROJECT_ID + ",\n"
                + "        \"name\": \"Test project\"\n"
                + "      },\n"
                + "      \"issue\": {\n"
                + "        \"id\": 53385\n"
                + "      },\n"
                + "      \"user\": {\n"
                + "        \"id\": 234,\n"
                + "        \"name\": \"Vardenis Pavardenis\"\n"
                + "      },\n"
                + "      \"activity\": {\n"
                + "        \"id\": 18,\n"
                + "        \"name\": \"Meetings & Team activities\"\n"
                + "      },\n"
                + "      \"hours\": 0.3,\n"
                + "      \"comments\": \"Stand up\",\n"
                + "      \"spent_on\": \"2020-11-24\",\n"
                + "      \"created_on\": \"2020-11-24T11:42:10Z\",\n"
                + "      \"updated_on\": \"2020-11-24T11:42:10Z\"\n"
                + "    }\n"
                + "  ],\n"
                + "  \"total_count\": 1,\n"
                + "  \"offset\": 0,\n"
                + "  \"limit\": 25\n"
                + "}";
    }
}
