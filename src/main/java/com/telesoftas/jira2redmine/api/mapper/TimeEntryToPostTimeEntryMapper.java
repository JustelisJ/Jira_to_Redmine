package com.telesoftas.jira2redmine.api.mapper;

import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.model.redminedata.wrappers.PostTimeEntry;
import com.telesoftas.jira2redmine.api.model.redminedata.wrappers.PostTimeEntry.PostTimeEntryBuilder;
import lombok.experimental.UtilityClass;

@UtilityClass
public class TimeEntryToPostTimeEntryMapper {
    public static PostTimeEntry mapToPostTimeEntry(TimeEntry timeEntry) {
        PostTimeEntryBuilder builder = PostTimeEntry.builder()
                .comments(timeEntry.getComments())
                .hours(timeEntry.getHours())
                .spentOn(timeEntry.getSpentOn());

        timeEntry.getUserId().ifPresent(builder::userId);
        timeEntry.getActivityId().ifPresent(builder::activityId);
        timeEntry.getProjectId().ifPresent(builder::projectId);

        return builder.build();
    }
}
