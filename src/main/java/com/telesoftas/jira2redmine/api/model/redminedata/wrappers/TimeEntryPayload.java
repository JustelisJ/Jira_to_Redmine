package com.telesoftas.jira2redmine.api.model.redminedata.wrappers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeEntryPayload<E> {
    E timeEntry;
}
