package com.telesoftas.jira2redmine.api.service.activity;

import com.telesoftas.jira2redmine.api.model.redminedata.Activity;

public interface ActivityService {

    Activity findActivityFromName(String name);

}
