package com.telesoftas.jira2redmine.api.model.jira;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class JiraWorkLog {

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private String accountId;
    private String description;
    private int timeSpentSeconds;
    private LocalDate startDate;
    private String issueId;
    private String projectKey;
    private String activity;

    @JsonSetter("startDate")
    public void setStartDate(String startDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        this.startDate = LocalDate.parse(startDate, formatter);
    }

    @JsonGetter("startDate")
    public String getStartDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        return startDate.format(formatter);
    }

}
