package com.telesoftas.jira2redmine.api.mapper;

import com.telesoftas.jira2redmine.api.exception.ProjectNotFoundException;
import com.telesoftas.jira2redmine.api.exception.UserNotFoundException;
import com.telesoftas.jira2redmine.api.json.model.FailedMapping;
import com.telesoftas.jira2redmine.api.json.writter.FailedPostWriterToFile;
import com.telesoftas.jira2redmine.api.model.imports.Worklog;
import com.telesoftas.jira2redmine.api.model.jira.JiraPackage;
import com.telesoftas.jira2redmine.api.model.jira.JiraWorkLog;
import com.telesoftas.jira2redmine.api.model.redminedata.*;
import com.telesoftas.jira2redmine.api.model.userMapping.entity.User;
import com.telesoftas.jira2redmine.api.repository.UserRepository;
import com.telesoftas.jira2redmine.api.service.activity.ActivityService;
import com.telesoftas.jira2redmine.api.service.issue.IssueService;
import com.telesoftas.jira2redmine.api.service.user.UserService;
import lombok.extern.log4j.Log4j2;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Log4j2
public class JiraPackageToTimeEntriesMapper {

    private static final int SECONDS_IN_MINUTE = 60;
    private static final int MINUTES_IN_HOUR = 60;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final String CDEV_PROJECT_KEY = "CDEV";
    public static final String CUBE_PROKECT_KEY = "CUBE";
    public static final long JAVA_MASTER_PROJECT_ID = 846L;
    public static final long STAGE_TECH_ID = 802L;
    public static final String MAPPING_ERROR = "Problem while mapping, check logs";

    private final IssueService issueService;
    private final ActivityService activityService;
    private final JiraUsersToRedmineUsersMapper mapper;
    private final UserRepository userRepository;
    private final UserService userService;

    private final FailedPostWriterToFile writter;

    public JiraPackageToTimeEntriesMapper(IssueService issueService,
                                          ActivityService activityService,
                                          JiraUsersToRedmineUsersMapper jiraUserMapper,
                                          UserRepository userRepository,
                                          UserService userService) {
        this.issueService = issueService;
        this.activityService = activityService;
        mapper = jiraUserMapper;
        this.userRepository = userRepository;
        this.userService = userService;

        writter = new FailedPostWriterToFile();
    }

    public List<TimeEntry> mapToTimeEntries(JiraPackage jiraPackage) {
        List<TimeEntry> timeEntries = new ArrayList<>();
        List<FailedMapping> incorrectWorkLogs = new ArrayList<>();

        for (JiraWorkLog workLog : jiraPackage.getWorkLogs()) {
            try {
                timeEntries.add(jiraWorklogToTimeEntry(workLog));
            } catch (UserNotFoundException e) {
                log.error("Couldn't map RedmineUser with ID " + workLog.getAccountId());
                incorrectWorkLogs.add(new FailedMapping(workLog, e.getMessage()));
            } catch (ProjectNotFoundException e) {
                log.error("Couldn't map Project with ID " + workLog.getIssueId());
                incorrectWorkLogs.add(new FailedMapping(workLog, e.getMessage()));
            }
        }

        if (!incorrectWorkLogs.isEmpty()) {
            writter.logWrongObjectToFile(incorrectWorkLogs);
        }
        return timeEntries;
    }

    public List<TimeEntry> mapToTimeEntries(Collection<Worklog> worklogs) {
        List<TimeEntry> timeEntries = new ArrayList<>();
        List<FailedMapping> incorrectWorkLogs = new ArrayList<>();

        for (Worklog workLog : worklogs) {
            Optional<TimeEntry> optionalTimeEntry = mapToTimeEntry(workLog);
            optionalTimeEntry.ifPresentOrElse(timeEntries::add,
                    () -> incorrectWorkLogs.add(new FailedMapping(workLog, MAPPING_ERROR)));
        }

        if (!incorrectWorkLogs.isEmpty()) {
            writter.logWrongObjectToFile(incorrectWorkLogs);
        }
        return timeEntries;
    }

    public Optional<TimeEntry> mapToTimeEntry(Worklog worklog) {
        try {
            return Optional.of(redmineWorklogToTimeEntry(worklog));
        } catch (UserNotFoundException e) {
            log.error("Couldn't map RedmineUser with ID " + worklog.getAccountId());
            return Optional.empty();
        } catch (ProjectNotFoundException e) {
            log.error("Couldn't map Project with ID " + worklog.getIssueID());
            return Optional.empty();
        }
    }

    private TimeEntry jiraWorklogToTimeEntry(JiraWorkLog workLog) throws UserNotFoundException, ProjectNotFoundException {
        return TimeEntry.builder()
                .user(findUser(workLog.getAccountId()))
                .activity(findActivity(workLog.getActivity()))
                .issue(findIssue(workLog.getIssueId()))
                .comments(findComment(workLog.getDescription()))
                .hours(findHours(workLog.getTimeSpentSeconds()))
                .project(findProject(workLog.getProjectKey()))
                .spentOn(findDate(workLog.getStartDate()))
                .build();
    }

    private TimeEntry redmineWorklogToTimeEntry(Worklog workLog) throws UserNotFoundException, ProjectNotFoundException {
        return TimeEntry.builder()
                .user(findUser(workLog.getAccountId()))
                .activity(findActivity(workLog.getActivity()))
                .issue(findIssue(workLog.getIssueID()))
                .comments(findComment(workLog.getDescription()))
                .hours(findHours(workLog.getTimeSpentSeconds()))
                .project(findProject(workLog.getProjectKey()))
                .spentOn(workLog.getStartDate())
                .build();
    }

    private Project findProject(String projectKey) throws ProjectNotFoundException {
        Project backendMasterProject = new Project(JAVA_MASTER_PROJECT_ID, "JAVA Master Project");
        Project stageTech = new Project(STAGE_TECH_ID, "Stage Tech");

        if (CDEV_PROJECT_KEY.equals(projectKey)) {
            return backendMasterProject;
        }
        if (CUBE_PROKECT_KEY.equals(projectKey)) {
            return stageTech;
        }
        log.error("Unable to find project");
        throw new ProjectNotFoundException();
    }

    private LocalDate findDate(String date) {
        return LocalDate.parse(date, formatter);
    }

    private String findComment(String description) {
        return description;
    }

    private double findHours(int seconds) {
        return secondsToHoursConverter(seconds);
    }

    private RedmineUser findUser(String accountId) throws UserNotFoundException {
        User user = userRepository.findById(accountId).orElseThrow(UserNotFoundException::new);
        return userService.findUserById(user.getRedmineUserId()).orElseThrow(UserNotFoundException::new);
    }

    private Issue findIssue(String issueId) {
        return issueService.getIssueFromName(issueId).orElseGet(Issue::new);
    }

    private Activity findActivity(String activity) {
        return activityService.findActivityFromName(activity);
    }

    private double secondsToHoursConverter(int seconds) {
        return (double) seconds / SECONDS_IN_MINUTE / MINUTES_IN_HOUR;
    }

}
