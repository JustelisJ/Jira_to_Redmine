package com.telesoftas.jira2redmine.api.service.timeentry.session;

import com.telesoftas.jira2redmine.api.model.imports.ImportStatus;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;

import java.time.LocalDate;
import java.util.List;

public interface SessionTimeEntryPost {

    ImportStatus importTimeEntries(LocalDate from, LocalDate to);

    boolean checkIfImportIsComplete(ImportStatus importStatus);

    List<TimeEntry> fetchCompletePost(ImportStatus importStatus);

    int importedWorklogCount(ImportStatus importStatus);

}
