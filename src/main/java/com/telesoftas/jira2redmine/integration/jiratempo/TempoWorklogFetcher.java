package com.telesoftas.jira2redmine.integration.jiratempo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.telesoftas.jira2redmine.contract.FetchWorklogs;
import com.telesoftas.jira2redmine.contract.FetchWorklogsError;
import com.telesoftas.jira2redmine.contract.JiraWorklog;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;


public class TempoWorklogFetcher implements FetchWorklogs {
    private final String tempoToken;
    private final int limit;

    public TempoWorklogFetcher(String tempoToken, int limit) {
        this.tempoToken = tempoToken;
        this.limit = limit;
    }

    @Override
    public Collection<JiraWorklog> fetchWorklogs(LocalDate from, LocalDate to, String... projectKeys)
            throws FetchWorklogsError {
        Collection<JiraWorklog> ourWorklogs = new ArrayList<>();

        for (String project : projectKeys) {
            try {
                Collection<JiraWorklog> projectWorklogs = fetch(from, to, project);
                for (JiraWorklog worklogs : projectWorklogs) {
                    worklogs.setProjectKey(project);
                }
                ourWorklogs.addAll(projectWorklogs);
            } catch (JsonProcessingException e) {
                throw new FetchWorklogsError(e);
            }
        }
        return ourWorklogs;
    }

    private HttpUrl buildUrl(LocalDate from, LocalDate to, String projectKey) {

        return HttpUrl.parse("https://api.tempo.io/core/3/worklogs").newBuilder()
                .addQueryParameter("from", String.valueOf(from))
                .addQueryParameter("to", String.valueOf(to))
                .addQueryParameter("limit", String.valueOf(limit))
                .addQueryParameter("project", String.valueOf(projectKey))
                .build();
    }

    private Collection<JiraWorklog> buildWorklog(String response) throws JsonProcessingException {
        Collection<JiraWorklog> ourWorklogs = new ArrayList<>();

        JsonNode actualObj = Utils.mapper.readTree(response);
        JsonNode jsonNode = actualObj.get("results");

        for (JsonNode node : jsonNode) {
            JiraWorklog worklog = JiraWorklog.builder()
                    .timeSpentSeconds(node.get("timeSpentSeconds").asInt())
                    .description(node.get("description").asText())
                    .startDate(LocalDate.parse(node.get("startDate").asText()))
                    .issueID(node.get("issue").get("key").asText())
                    .accountId(node.get("author").get("accountId").asText())
                    .build();

            JsonNode attributesNode = node.get("attributes");
            JsonNode activityNode = attributesNode.get("values");
            if (activityNode.isEmpty()) {
                worklog.setActivity(null);
            } else {
                for (JsonNode j : activityNode) {
                    String activity = j.get("key").asText();
                    if (activity.equals("_Activitygroup_")) {
                        worklog.setActivity(j.get("value").asText());
                        // We expect to have only one key and value pair
                        break;
                    }
                }
            }
            ourWorklogs.add(worklog);
        }
        return ourWorklogs;
    }

    private String getResponseFromTempo(HttpUrl url) {


        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", tempoToken)
                .build();
        Response response;
        String responseBody;

        try {
            response = client.newCall(request).execute();
            responseBody = response.body().string();


        } catch (IllegalArgumentException | IOException e) {
            throw new InvalidUrlException("Invalid URL", e);
        }
        return responseBody;
    }

    private Collection<JiraWorklog> fetch(HttpUrl url, Collection<JiraWorklog> projectWorklogs) throws JsonProcessingException {

        String response = getResponseFromTempo(url);

        projectWorklogs.addAll(buildWorklog(response));

        HttpUrl nextUrl = getNextLink(response);

        if (nextUrl != null) {
            return fetch(nextUrl, projectWorklogs);
        } else {
            return projectWorklogs;
        }

    }

    private HttpUrl getNextLink(String response) throws JsonProcessingException {
        JsonNode actualObj;
        actualObj = Utils.mapper.readTree(response);

        JsonNode jsonNode = actualObj.get("metadata");
        if (jsonNode.get("next") != null) {
            String url = jsonNode.get("next").asText();
            return HttpUrl.parse(url);
        } else {
            return null;
        }
    }

    private Collection<JiraWorklog> fetch(LocalDate from, LocalDate to, String project) throws JsonProcessingException {
        HttpUrl url = buildUrl(from, to, project);

        return fetch(url, new ArrayList<>());
    }
}
