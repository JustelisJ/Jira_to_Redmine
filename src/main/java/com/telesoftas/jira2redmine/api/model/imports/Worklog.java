package com.telesoftas.jira2redmine.api.model.imports;

import com.telesoftas.jira2redmine.contract.JiraWorklog;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Worklog {

    @Id
    private Long worklogID;

    private Long timeEntryID;
    private String accountId;
    private String description;
    private int timeSpentSeconds;
    private LocalDate startDate;
    private String issueID;
    private String projectKey;
    private String activity;

    public Worklog(JiraWorklog worklog) {
        accountId = worklog.getAccountId();
        description = worklog.getDescription();
        timeSpentSeconds = worklog.getTimeSpentSeconds();
        startDate = worklog.getStartDate();
        issueID = worklog.getIssueID();
        projectKey = worklog.getProjectKey();
        activity = worklog.getActivity();
        worklogID = worklog.getWorklogID();
    }

    public Worklog(JiraWorklog worklog, Long timeEntryID) {
        accountId = worklog.getAccountId();
        description = worklog.getDescription();
        timeSpentSeconds = worklog.getTimeSpentSeconds();
        startDate = worklog.getStartDate();
        issueID = worklog.getIssueID();
        projectKey = worklog.getProjectKey();
        activity = worklog.getActivity();
        worklogID = worklog.getWorklogID();
        this.timeEntryID = timeEntryID;
    }

    public boolean equalsTo(JiraWorklog worklog) {
        if (worklogID != null ? !worklogID.equals(worklog.getWorklogID())
                : worklog.getWorklogID() != null) return false;
        if (accountId != null ? !accountId.equals(worklog.getAccountId()) : worklog.getAccountId() != null)
            return false;
        if (description != null ? !description.equals(worklog.getDescription()) : worklog.getDescription() != null)
            return false;
        if (timeSpentSeconds != worklog.getTimeSpentSeconds()) return false;
        if (startDate != null ? !startDate.equals(worklog.getStartDate()) : worklog.getStartDate() != null)
            return false;
        if (issueID != null ? !issueID.equals(worklog.getIssueID()) : worklog.getIssueID() != null) return false;
        if (projectKey != null ? !projectKey.equals(worklog.getProjectKey()) : worklog.getProjectKey() != null)
            return false;
        return activity != null ? activity.equals(worklog.getActivity()) : worklog.getActivity() == null;
    }

    public boolean isNew() {
        return timeEntryID == null;
    }
}
