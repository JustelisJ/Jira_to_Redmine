package com.telesoftas.jira2redmine.api.model.redminedata;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RedmineUser {

    private Long id;
    private String login;
    private boolean admin;
    private String firstname;
    private String lastname;
    private String mail;
    private LocalDate createdOn;
    private LocalDate lastLoginOn;

    @JsonSetter("name")
    public void splitNameToFirstnameAndLastname(String name) {
        String[] nameSplit = name.split(" ");
        firstname = nameSplit[0];
        lastname = nameSplit[1];
    }

    public String getFullName() {
        StringBuilder builder = new StringBuilder();
        if (firstname != null) {
            builder.append(firstname);
        }
        if (lastname != null) {
            builder.append(" ").append(lastname);
        }
        return builder.toString();
    }

}
