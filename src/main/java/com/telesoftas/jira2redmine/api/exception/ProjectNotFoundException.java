package com.telesoftas.jira2redmine.api.exception;

public class ProjectNotFoundException extends Exception {

    public static final String PROJECT_NOT_FOUNT_MESSAGE = "No such project exists";
    public static final String PROJECT_DOES_NOT_EXIST = "\" project does not exist";

    public ProjectNotFoundException() {
        super(PROJECT_NOT_FOUNT_MESSAGE);
    }

    public ProjectNotFoundException(String name) {
        super("\"" + name + PROJECT_DOES_NOT_EXIST);
    }

}
