package com.telesoftas.jira2redmine.api.model.redminedata.wrappers;

import com.telesoftas.jira2redmine.api.model.redminedata.Issue;
import lombok.Data;

import java.util.List;

@Data
public class IssueData extends Metadata {

    private List<Issue> issues;

}
