package com.telesoftas.jira2redmine.integration.jiratempo;

import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class JiraUserEmailFetcher {

    private final String jiraDomain;
    private final String jiraToken;

    public JiraUserEmailFetcher(String jiraDomain, String jiraToken) {
        this.jiraDomain = jiraDomain;
        this.jiraToken = jiraToken;
    }

    private HttpUrl buildJiraUrl(int startAt, int maxResults) {

        return new HttpUrl.Builder()
                .scheme("https")
                .host(jiraDomain + ".atlassian.net")
                .addPathSegment("rest")
                .addPathSegment("api")
                .addPathSegment("3")
                .addPathSegment("users")
                .addPathSegment("search")
                .addQueryParameter("startAt", String.valueOf(startAt))
                .addQueryParameter("maxResults", String.valueOf(maxResults))
                .build();
    }


    public String getResponse(int startAt, int maxResults) throws IOException {
        HttpUrl url = buildJiraUrl(startAt, maxResults);
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", jiraToken)
                .build();
        Response response;
        String myResponse;

        response = client.newCall(request).execute();
        myResponse = response.body().string();

        return myResponse;
    }


}
