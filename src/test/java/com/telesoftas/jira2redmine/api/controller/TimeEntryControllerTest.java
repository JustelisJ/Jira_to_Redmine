package com.telesoftas.jira2redmine.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telesoftas.jira2redmine.api.mapper.JiraPackageToTimeEntriesMapper;
import com.telesoftas.jira2redmine.api.model.imports.ImportStatus;
import com.telesoftas.jira2redmine.api.model.jira.JiraPackage;
import com.telesoftas.jira2redmine.api.model.jira.JiraWorkLog;
import com.telesoftas.jira2redmine.api.model.redminedata.Issue;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;
import com.telesoftas.jira2redmine.api.service.timeentry.TimeEntryService;
import com.telesoftas.jira2redmine.api.service.timeentry.session.SessionTimeEntryPost;
import com.telesoftas.jira2redmine.contract.JiraWorklog;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.util.*;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class TimeEntryControllerTest {

    private static final String GET_ISSUES_URL = "/timeEntries";
    private static final String ISSUES_VIEW_NAME = "timeEntry/displayTimeEntries";
    private static final String TIME_ENTRY_ATTRIBUTE_NAME = "timeEntries";
    private static final String TIME_ENTRIES_NEW_URL = "/timeEntries/new";
    private static final String REDIRECT_TIME_ENTRIES_URL = "redirect:/timeEntries";

    @Mock
    TimeEntryService entryService;
    @Mock
    SessionTimeEntryPost asyncPost;
    @Mock
    JiraPackageToTimeEntriesMapper jiraMapper;
    @InjectMocks
    TimeEntryController controller;

    ObjectMapper mapper;
    List<TimeEntry> returnedTimeEntries;
    JiraPackage jiraPackage;
    List<TimeEntry> timeEntries;
    Collection<JiraWorklog> fetchedWorkLogs;

    Issue issue;
    RedmineUser user;

    MockMvc mockMvc;

    private void initializeMock() {

        controller = new TimeEntryController(entryService, asyncPost, jiraMapper);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    private void initializeData() {
        initializeReturnedTimeEntry();

        initializeJiraPackage();

        mapper = new ObjectMapper();

        timeEntries = new ArrayList<>();
        timeEntries.add(TimeEntry.builder().id(1L).build());

        initializeIssue();

        initializeUser();

        initializeFetchedWorkLogs();
    }

    private void initializeFetchedWorkLogs() {
        fetchedWorkLogs = new ArrayList<>();
        JiraWorklog worklog = JiraWorklog.builder()
                .projectKey("CDEV")
                .startDate(LocalDate.of(2020, 12, 7))
                .build();
        fetchedWorkLogs.add(worklog);
    }

    private void initializeUser() {
        user = new RedmineUser();
        user.setId(1L);
    }

    private void initializeIssue() {
        issue = new Issue();
        issue.setId(1L);
    }

    private void initializeJiraPackage() {
        JiraWorkLog workLog = new JiraWorkLog();
        workLog.setAccountId("5f9a8e6762584c006be3447c");
        workLog.setDescription("Working on issue TP-1");
        workLog.setTimeSpentSeconds(3600);
        workLog.setStartDate("2020-10-31");
        workLog.setIssueId("NP-1");
        workLog.setActivity("Coding");
        workLog.setProjectKey("CUBE");

        Set<JiraWorkLog> workLogs = new HashSet<>();
        workLogs.add(workLog);
        jiraPackage = new JiraPackage();
        jiraPackage.setWorkLogs(workLogs);
    }

    private void initializeReturnedTimeEntry() {
        returnedTimeEntries = new ArrayList<>();
        returnedTimeEntries.add(TimeEntry.builder().id(1L).build());
        returnedTimeEntries.add(TimeEntry.builder().id(2L).build());
    }

    @BeforeEach
    void setUp() {
        initializeData();
        initializeMock();
    }

    @Test
    void getIssues() throws Exception {
        when(entryService.getTimeEntries()).thenReturn(returnedTimeEntries);
        mockMvc.perform(get(GET_ISSUES_URL))
                .andExpect(status().isOk())
                .andExpect(view().name(ISSUES_VIEW_NAME))
                .andExpect(model().attributeExists(TIME_ENTRY_ATTRIBUTE_NAME));
        verify(entryService, times(1)).getTimeEntries();
    }

    @Test
    void createTimeEntryFromJSON() throws Exception {
        when(entryService.postTimeEntries(anyList())).thenReturn(timeEntries);
        when(jiraMapper.mapToTimeEntries(any(JiraPackage.class))).thenReturn(timeEntries);
        mockMvc.perform(post(TIME_ENTRIES_NEW_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(jiraPackage))
        )
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name(REDIRECT_TIME_ENTRIES_URL));
        verify(entryService, times(1)).postTimeEntries(anyList());
    }

    @Test
    void checkIfPostRequestHasFinished() throws Exception {
        List<TimeEntry> timeEntries = new ArrayList<>();
        timeEntries.add(TimeEntry.builder().id(1L).build());
        when(asyncPost.fetchCompletePost(any(ImportStatus.class)))
                .thenReturn(timeEntries);
        when(asyncPost.checkIfImportIsComplete(any(ImportStatus.class))).thenReturn(true);

        mockMvc.perform(post("/timeEntries/import")
                .flashAttr("from", LocalDate.of(2020, 12, 1))
                .flashAttr("to", LocalDate.of(2020, 12, 30)))
                .andExpect(status().isOk())
                .andExpect(view().name("timeEntry/timeEntryTable"))
                .andExpect(model().attributeExists("timeEntries"));
    }

    @Test
    void checkIfPostRequestHasFinishedButItsStillInProgress() throws Exception {
        when(asyncPost.checkIfImportIsComplete(any(ImportStatus.class))).thenReturn(false);

        mockMvc.perform(post("/timeEntries/import")
                .flashAttr("from", LocalDate.of(2020, 12, 1))
                .flashAttr("to", LocalDate.of(2020, 12, 30)))
                .andExpect(status().isOk())
                .andExpect(view().name("timeEntry/timeEntryTable"))
                .andExpect(model().attributeDoesNotExist("timeEntries"));
    }
}
