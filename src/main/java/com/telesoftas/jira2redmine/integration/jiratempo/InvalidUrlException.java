package com.telesoftas.jira2redmine.integration.jiratempo;

public class InvalidUrlException extends RuntimeException {

    public InvalidUrlException(String message, Throwable throwable) {
        super(message, throwable);
    }


}
