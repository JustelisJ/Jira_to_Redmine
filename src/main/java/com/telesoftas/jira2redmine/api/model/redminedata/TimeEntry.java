package com.telesoftas.jira2redmine.api.model.redminedata;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TimeEntry {

    private Long id;
    private Project project;
    private Issue issue;
    private RedmineUser user;
    private Activity activity;
    private double hours;
    private String comments;
    private LocalDate spentOn;
    private LocalDate createdOn;
    private LocalDate updatedOn;

    public Optional<Long> getUserId() {
        return ofNullable(user).map(RedmineUser::getId);
    }

    public Optional<Long> getActivityId() {
        return ofNullable(activity).map(Activity::getId);
    }

    public Optional<Long> getIssueId() {
        return ofNullable(issue).map(Issue::getId);
    }

    public Optional<Long> getProjectId() {
        return ofNullable(project).map(Project::getId);
    }
}
