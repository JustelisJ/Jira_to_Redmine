package com.telesoftas.jira2redmine.api.service.issue;

import com.telesoftas.jira2redmine.api.model.redminedata.Issue;
import com.telesoftas.jira2redmine.api.model.redminedata.wrappers.IssueData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class RedmineIssueServiceTest {

    private static final String NP_1_ISSUE_NAME = "NP-1";
    private static final String NP_3_ISSUE_NAME = "NP-3";
    private static final long ISSUE_ID = 2L;

    private static final Issue issue = Issue.builder()
            .id(ISSUE_ID)
            .subject(NP_1_ISSUE_NAME)
            .build();

    @Mock
    RestTemplate restTemplate;

    IssueService issueService;

    IssueData issueData;
    ResponseEntity<IssueData> responseEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        initializeIssueData();
        initializeResponseEntity();
        whenThenForRestTemplate();
        issueService = new RedmineIssueService(restTemplate);
        ((RedmineIssueService) issueService).postConstruct();
    }

    private void whenThenForRestTemplate() {
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class),
                eq(IssueData.class), anyMap()))
                .thenReturn(responseEntity);
    }

    private void initializeResponseEntity() {
        responseEntity = new ResponseEntity<>(issueData, HttpStatus.ACCEPTED);
    }

    private void initializeIssueData() {
        issueData = new IssueData();
        List<Issue> issues = new ArrayList<>();

        issues.add(issue);
        issueData.setIssues(issues);

        issueData.setTotalCount(1);
    }

    @Test
    void getIssueIdFromName() {
        Optional<Issue> id = issueService.getIssueFromName(NP_1_ISSUE_NAME);
        assertTrue(id.isPresent());
        assertEquals(issue, id.get());
    }

    @Test
    void dontGetIssueIdFromName() {
        Optional<Issue> id = issueService.getIssueFromName(NP_3_ISSUE_NAME);
        assertTrue(id.isEmpty());
    }
}
