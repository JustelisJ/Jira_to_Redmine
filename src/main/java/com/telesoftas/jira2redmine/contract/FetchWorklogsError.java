package com.telesoftas.jira2redmine.contract;

import java.io.IOException;

public class FetchWorklogsError extends IOException {

    public FetchWorklogsError() {
        super();
    }

    public FetchWorklogsError(String message) {
        super(message);
    }

    public FetchWorklogsError(String message, Throwable throwable) {
        super(message, throwable);
    }

    public FetchWorklogsError(Throwable throwable) {
        super(throwable);
    }

}
