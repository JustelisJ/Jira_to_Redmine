package com.telesoftas.jira2redmine.api.service.user.mapping;

import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;
import com.telesoftas.jira2redmine.api.model.userMapping.JiraUser;
import com.telesoftas.jira2redmine.api.model.userMapping.entity.User;
import com.telesoftas.jira2redmine.api.repository.UserRepository;
import com.telesoftas.jira2redmine.api.service.user.UserService;
import com.telesoftas.jira2redmine.contract.FetchWorklogs;
import com.telesoftas.jira2redmine.contract.FetchWorklogsError;
import com.telesoftas.jira2redmine.contract.JiraWorklog;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class UserMappingServiceTest {

    private static final RedmineUser EMPTY_USER = RedmineUser.builder().build();
    public static final String FULL_NAME = "Vardenis Pavardenis";
    public static final String ACCOUNT_ID = "asd123456";
    public static final String FIRSTNAME = "Vardenis";
    public static final String LASTNAME = "Pavardenis";

    @Mock
    FetchWorklogs fetchWorklogs;
    @Mock
    UserRepository userRepository;
    @Mock
    UserService userService;
    UserMappingService mappingService;

    JiraUser jiraUser;
    RedmineUser user;
    LocalDate from;
    LocalDate to;
    Collection<JiraWorklog> jiraWorklogs;
    Map<JiraUser, RedmineUser> userMap;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        mappingService = new UserMappingService(fetchWorklogs, userRepository, userService);

        jiraUser = new JiraUser(FULL_NAME, ACCOUNT_ID);
        user = RedmineUser.builder()
                .firstname(FIRSTNAME)
                .lastname(LASTNAME)
                .build();
        from = LocalDate.of(2020, 1, 1);
        to = LocalDate.of(2020, 1, 31);
        jiraWorklogs = new ArrayList<>();
        jiraWorklogs.add(JiraWorklog.builder()
                .displayName(FULL_NAME)
                .accountId(ACCOUNT_ID)
                .build());
        userMap = new HashMap<>();
    }

    @Test
    void getMappedUsers_jiraWorklogFetchingFails() throws FetchWorklogsError {
        when(fetchWorklogs.fetchWorklogs(any(LocalDate.class), any(LocalDate.class), any(String.class), any(String.class)))
                .thenThrow(FetchWorklogsError.class);

        Map<JiraUser, RedmineUser> mappedUsers = mappingService.getMappedUsers(from, to);

        assertEquals(new HashMap<>(), mappedUsers);
    }

    @Test
    void getMappedUsers_userNotSaveToDatabase() throws FetchWorklogsError {
        userMap.put(jiraUser, EMPTY_USER);
        when(fetchWorklogs.fetchWorklogs(any(LocalDate.class), any(LocalDate.class), any(String.class), any(String.class)))
                .thenReturn(jiraWorklogs);
        when(userRepository.findById(anyString())).thenReturn(Optional.empty());

        Map<JiraUser, RedmineUser> mappedUsers = mappingService.getMappedUsers(from, to);

        assertEquals(userMap, mappedUsers);
    }

    @Test
    void getMappedUsers_userSavedToDatabase() throws FetchWorklogsError {
        userMap.put(jiraUser, user);
        when(fetchWorklogs.fetchWorklogs(any(LocalDate.class), any(LocalDate.class),
                any(String.class), any(String.class))).thenReturn(jiraWorklogs);
        when(userRepository.findById(anyString()))
                .thenReturn(Optional.of(User.builder().redmineUserId(1L).build()));
        when(userService.findUserById(anyLong())).thenReturn(Optional.of(user));

        Map<JiraUser, RedmineUser> mappedUsers = mappingService.getMappedUsers(from, to);

        assertEquals(userMap, mappedUsers);
    }

}