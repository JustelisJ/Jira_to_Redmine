package com.telesoftas.jira2redmine.api.model.redminedata;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Activity {

    private Long id;
    private String name;
    private boolean isDefault;
    private boolean active;

    public Activity(long id, String name) {
        this(id, name, false, false);
    }

    @JsonSetter("is_default")
    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

}
