package com.telesoftas.jira2redmine.integration.jiratempo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.HashMap;
import java.util.Map;

public class JiraUserDataParser {

    private final Map<String, String> map = new HashMap<>();

    public Map<String, String> parseUserData(String response) throws JsonProcessingException {

        JsonNode actualObj;

        actualObj = Utils.mapper.readTree(response);
        for (JsonNode node : actualObj) {
            JsonNode accountIdNode = node.get("accountId");
            JsonNode displayNameNode = node.get("displayName");

            if (accountIdNode != null && displayNameNode != null) {
                String accountId = accountIdNode.asText();
                String displayName = displayNameNode.asText();
                map.put(accountId, displayName);
            }
        }

        return map;
    }
}
