package com.telesoftas.jira2redmine.api.config;

import com.telesoftas.jira2redmine.api.mapper.JiraPackageToTimeEntriesMapper;
import com.telesoftas.jira2redmine.api.mapper.JiraUsersToRedmineUsersMapper;
import com.telesoftas.jira2redmine.api.repository.UserRepository;
import com.telesoftas.jira2redmine.api.service.activity.ActivityService;
import com.telesoftas.jira2redmine.api.service.issue.IssueService;
import com.telesoftas.jira2redmine.api.service.user.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {

    @Bean
    public JiraPackageToTimeEntriesMapper jiraPackageToTimeEntriesMapper(IssueService issueService,
                                                                         ActivityService activityService,
                                                                         JiraUsersToRedmineUsersMapper jiraUsersToRedmineUsersMapper,
                                                                         UserRepository userRepository,
                                                                         UserService userService) {
        return new JiraPackageToTimeEntriesMapper(issueService, activityService, jiraUsersToRedmineUsersMapper, userRepository, userService);
    }

    @Bean
    public JiraUsersToRedmineUsersMapper jiraUsersToRedmineUsersMapper(UserService userService) {
        return new JiraUsersToRedmineUsersMapper(userService);
    }
}
