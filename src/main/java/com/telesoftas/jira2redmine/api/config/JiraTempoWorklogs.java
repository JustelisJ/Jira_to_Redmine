package com.telesoftas.jira2redmine.api.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telesoftas.jira2redmine.api.config.JiraTempoWorklogs.JiraConfig;
import com.telesoftas.jira2redmine.api.config.JiraTempoWorklogs.TempoConfig;
import com.telesoftas.jira2redmine.contract.JiraWorklog;
import com.telesoftas.jira2redmine.integration.jiratempo.JiraUserEmailFetcher;
import com.telesoftas.jira2redmine.integration.jiratempo.TempoWorklogFetcher;
import com.telesoftas.jira2redmine.integration.jiratempo.UsersIdToDisplayNameFetcher;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Configuration
@EnableConfigurationProperties({JiraConfig.class, TempoConfig.class})
public class JiraTempoWorklogs {

    @Profile("test")
    @Bean
    public TempoWorklogFetcher fetchWorklogs(TempoConfig tempo) {
        return new TempoWorklogFetcher(tempo.getToken(), 1000);
    }

    @Profile("dev")
    @Bean
    public TempoWorklogFetcher fetchDummyWorklogs(TempoConfig tempo) {
        return new TempoWorklogFetcher(tempo.getToken(), 1000) {
            @Override
            public Collection<JiraWorklog> fetchWorklogs(LocalDate from, LocalDate to, String... projectKeys) {
                List<JiraWorklog> worklogs = new ArrayList<>();

                JiraWorklog worklog = JiraWorklog.builder()
                        .worklogID(1L)
                        .startDate(LocalDate.of(2020, 10, 1))
                        .accountId("5f9a8e6762584c006be3447c")
                        .displayName("Justinas Jancys")
                        .description("Working on issue TP-1")
                        .issueID("test")
                        .activity("TS121withSTLHRTTL")
                        .timeSpentSeconds(3600)
                        .projectKey("CDEV")
                        .build();

                worklogs.add(worklog);

                JiraWorklog worklog1 = JiraWorklog.builder()
                        .worklogID(2L)
                        .startDate(LocalDate.of(2020, 10, 5))
                        .accountId("5f9a8e6762584c006be3447c")
                        .displayName("Justinas Jancys")
                        .description("Working on issue TP-1")
                        .issueID("test")
                        .activity("TS121withSTLHRTTL")
                        .timeSpentSeconds(3600)
                        .projectKey("CDEV")
                        .build();

                worklogs.add(worklog1);

                JiraWorklog incorrectWorklog = JiraWorklog.builder()
                        .worklogID(3L)
                        .startDate(LocalDate.of(2020, 10, 6))
                        .accountId("5f9a8e6762584c00aaaaaa")    //incorrect data
                        .displayName("Vardenis Pavardenis")
                        .description("Working on issue TP-1")
                        .issueID("test")
                        .activity("TS121withSTLHRTTL")
                        .timeSpentSeconds(3600)
                        .projectKey("Project key")  //incorrect data
                        .build();

                worklogs.add(incorrectWorklog);

                return worklogs;
            }
        };
    }

    @Bean
    public JiraUserEmailFetcher fetchJiraUserEmail(JiraConfig jira) {
        return new JiraUserEmailFetcher(
                jira.getDomain(),
                jira.getToken()
        );
    }

    @Bean
    public UsersIdToDisplayNameFetcher fetchAllUsersIdToDisplayName(JiraUserEmailFetcher fetchJiraUserEmail) {
        return new UsersIdToDisplayNameFetcher(fetchJiraUserEmail, new ObjectMapper());
    }

    @Data
    @ConfigurationProperties(prefix = "jiratempo.jira")
    public static class JiraConfig {

        private String domain;
        private String token;
    }

    @Data
    @ConfigurationProperties(prefix = "jiratempo.tempo")
    public static class TempoConfig {

        private String token;
    }
}
