package com.telesoftas.jira2redmine.api.mapper;

import com.telesoftas.jira2redmine.api.model.jira.JiraPackage;
import com.telesoftas.jira2redmine.api.model.jira.JiraWorkLog;
import com.telesoftas.jira2redmine.api.model.redminedata.Activity;
import com.telesoftas.jira2redmine.api.model.redminedata.Issue;
import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.repository.UserRepository;
import com.telesoftas.jira2redmine.api.service.activity.ActivityService;
import com.telesoftas.jira2redmine.api.service.issue.IssueService;
import com.telesoftas.jira2redmine.api.service.user.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class JiraPackageToTimeEntriesMapperTest {

    @Mock
    IssueService issueService;
    @Mock
    UserService userService;
    @Mock
    ActivityService activityService;
    @Mock
    UserRepository userRepository;

    JiraPackageToTimeEntriesMapper timeEntriesMapper;

    JiraWorkLog correctWorkLog;
    JiraWorkLog incorrectWorkLog;
    JiraPackage jiraPackage;

    RedmineUser user;
    Issue issue;
    Activity activity;

    private void initializeData() {
        initializeCorrectJiraWorkLog();

        initializeIncorrectJiraWorkLog();

        initializeJiraPackage();

        user = RedmineUser.builder().id(7L).build();
        issue = Issue.builder().id(2L).build();
        activity = Activity.builder().id(4L).build();
    }

    private void initializeJiraPackage() {
        jiraPackage = new JiraPackage();
        jiraPackage.setWorkLogs(new HashSet<>());
    }

    private void initializeIncorrectJiraWorkLog() {
        incorrectWorkLog = new JiraWorkLog();
        incorrectWorkLog.setAccountId("wrong");
        incorrectWorkLog.setIssueId("2");
        incorrectWorkLog.setStartDate("2020-10-26");
        incorrectWorkLog.setTimeSpentSeconds(3600);
        incorrectWorkLog.setDescription("Documentation");
        incorrectWorkLog.setProjectKey("null");
    }

    private void initializeCorrectJiraWorkLog() {
        correctWorkLog = new JiraWorkLog();
        correctWorkLog.setAccountId("5f9a8e6762584c006be3447c");
        correctWorkLog.setIssueId("2");
        correctWorkLog.setStartDate("2020-10-26");
        correctWorkLog.setTimeSpentSeconds(3600);
        correctWorkLog.setDescription("Documentation");
        correctWorkLog.setActivity("some activity");
        correctWorkLog.setProjectKey(JiraPackageToTimeEntriesMapper.CDEV_PROJECT_KEY);
    }

    @BeforeEach
    void setUp() {
        initializeData();
        MockitoAnnotations.initMocks(this);
        when(userService.findUserByName(anyString())).thenReturn(Optional.of(user));
        timeEntriesMapper = new JiraPackageToTimeEntriesMapper(issueService, activityService,
                new JiraUsersToRedmineUsersMapper(userService), userRepository, userService);
    }

    @Test
    void mapToTimeEntries() {
        when(issueService.getIssueFromName(anyString())).thenReturn(Optional.of(issue));
        when(activityService.findActivityFromName(anyString())).thenReturn(activity);
        when(userService.findUserByName(anyString())).thenReturn(Optional.of(user));

        //adds the correct JiraWorkLog
        jiraPackage.getWorkLogs().add(correctWorkLog);
        List<TimeEntry> timeEntryList = timeEntriesMapper.mapToTimeEntries(jiraPackage);

        assertFalse(timeEntryList.isEmpty());
        assertEquals(user, timeEntryList.get(0).getUser());
        assertEquals(issue, timeEntryList.get(0).getIssue());
        assertEquals(activity, timeEntryList.get(0).getActivity());

        verify(userService, times(1)).findUserByName(anyString());
        verify(activityService, times(1)).findActivityFromName(anyString());
        verify(issueService, times(1)).getIssueFromName(anyString());
    }

    @Test
    void mapToTimeEntriesWithIncorrectAccountID() {
        when(issueService.getIssueFromName("wrong")).thenReturn(Optional.ofNullable(null));
        when(userService.findUserByName(anyString())).thenReturn(Optional.of(user));
        when(activityService.findActivityFromName(anyString())).thenReturn(activity);

        //adds the incorrect JiraWorkLog
        jiraPackage.getWorkLogs().add(incorrectWorkLog);

        List<TimeEntry> timeEntryList = timeEntriesMapper.mapToTimeEntries(jiraPackage);

        assertTrue(timeEntryList.isEmpty());
    }

    @Test
    void mapToTimeEntriesWithIncorrectIssueID() {
        when(issueService.getIssueFromName(anyString())).thenReturn(Optional.of(issue));
        when(userService.findUserByName(anyString())).thenReturn(Optional.ofNullable(null));
        when(activityService.findActivityFromName(anyString())).thenReturn(activity);

        //adds the correct JiraWorkLog
        jiraPackage.getWorkLogs().add(incorrectWorkLog);

        List<TimeEntry> timeEntryList = timeEntriesMapper.mapToTimeEntries(jiraPackage);

        assertTrue(timeEntryList.isEmpty());
    }
}
