package com.telesoftas.jira2redmine.contract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@AllArgsConstructor
@Data
@Builder
public class JiraWorklog {

    private Long worklogID;
    private String accountId;
    private String displayName;
    private String description;
    private int timeSpentSeconds;
    private LocalDate startDate;
    private String issueID;
    private String projectKey;
    private String activity;

}
