package com.telesoftas.jira2redmine.api.model.imports;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class ImportStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long sessionID;

    private ImportStatusTableStatus status = ImportStatusTableStatus.CREATED;
}
