package com.telesoftas.jira2redmine.api.model.redminedata.wrappers;

import lombok.Data;

@Data
public class Metadata {

    private int totalCount;
    private int offset;
    private int limit;

}
