package com.telesoftas.jira2redmine.api.service.timeentry.session;

import com.telesoftas.jira2redmine.api.mapper.JiraPackageToTimeEntriesMapper;
import com.telesoftas.jira2redmine.api.model.imports.*;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.repository.ImportToWorklogRepository;
import com.telesoftas.jira2redmine.api.repository.WorklogRepository;
import com.telesoftas.jira2redmine.api.service.timeentry.TimeEntryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class WorklogUpdaterTest {

    @Mock
    TimeEntryService timeEntryService;
    @Mock
    JiraPackageToTimeEntriesMapper mapper;
    @Mock
    WorklogRepository worklogRepository;
    @Mock
    ImportToWorklogRepository importToWorklogRepository;

    WorklogUpdater updater;

    ImportStatus importStatus;
    List<Worklog> worklogs;
    Worklog dbWorklog;
    TimeEntry timeEntry;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        updater = new WorklogUpdater(timeEntryService, mapper, worklogRepository, importToWorklogRepository);

        importStatus = new ImportStatus(1L, ImportStatusTableStatus.PENDING);
        dbWorklog = Worklog.builder()
                .worklogID(1L)
                .timeEntryID(2L)
                .build();
        worklogs = new ArrayList<>();
        worklogs.add(dbWorklog);
        timeEntry = TimeEntry.builder()
                .id(2L)
                .build();
    }

    @Test
    void update_FailToMap() {
        when(mapper.mapToTimeEntry(any(Worklog.class))).thenReturn(Optional.empty());

        updater.update(worklogs, importStatus);

        verify(importToWorklogRepository, times(1))
                .updateStatus(any(ImportID.class), eq(ImportToWorklogTableStatus.ERROR));
    }

    @Test
    void update_successfullyUpdate() {
        when(mapper.mapToTimeEntry(any(Worklog.class))).thenReturn(Optional.of(timeEntry));

        updater.update(worklogs, importStatus);

        verify(timeEntryService, times(1))
                .updateTimeEntry(any(TimeEntry.class), anyLong());
        verify(worklogRepository, times(1)).save(dbWorklog);
        verify(importToWorklogRepository, times(1))
                .updateStatus(any(ImportID.class), eq(ImportToWorklogTableStatus.IMPORTED));
    }
}