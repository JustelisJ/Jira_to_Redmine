package com.telesoftas.jira2redmine.api.service.activity;

import com.telesoftas.jira2redmine.api.model.redminedata.Activity;
import com.telesoftas.jira2redmine.api.model.redminedata.wrappers.ActivityData;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.*;

@Log4j2
@Service
public class RedmineActivityService implements ActivityService {

    private static final String COULDNT_MAP_MESSAGE = "Couldn't map activities";
    private static final String MAPPED_ACTIVITIES_MESSAGE = "Mapped all activities";
    public static final String ACTIVITY_SERVICE_ERROR = "Couldn't fetch activities from Activity service";

    private final String url;
    private final RestTemplate restTemplate;
    private Map<String, Activity> activities;

    public RedmineActivityService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.url = "/enumerations/time_entry_activities.json";
        activities = new HashMap<>();
    }

    @PostConstruct
    void postConstruct() {
        mapAllActivities();
    }

    @Override
    public Activity findActivityFromName(String name) {
        return activities.getOrDefault(name, activities.get("default"));
    }

    private Set<Activity> getActivities() {
        Set<Activity> activitySet = new HashSet<>();

        ResponseEntity<ActivityData> responseData;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        responseData = restTemplate.exchange(url, HttpMethod.GET, entity, ActivityData.class);
        Optional.ofNullable(responseData.getBody()).ifPresentOrElse(
                (activityData -> activitySet.addAll(activityData.getTimeEntryActivities())),
                (() -> log.error(ACTIVITY_SERVICE_ERROR)));
        return activitySet;
    }

    @Scheduled(fixedRate = 300000)
    private void mapAllActivities() {
        Set<Activity> activitySet = getActivities();
        if (!activitySet.isEmpty()) {
            for (Activity activity : activitySet) {
                activities.put(activity.getName(), activity);
                if (activity.isDefault()) {
                    activities.put("default", activity);
                }
            }
            log.info(MAPPED_ACTIVITIES_MESSAGE);
        } else {
            log.error(COULDNT_MAP_MESSAGE);
        }
    }
}
