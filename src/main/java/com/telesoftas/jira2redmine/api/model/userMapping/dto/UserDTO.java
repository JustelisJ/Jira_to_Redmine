package com.telesoftas.jira2redmine.api.model.userMapping.dto;

import com.telesoftas.jira2redmine.api.model.userMapping.entity.User;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class UserDTO {

    @NotNull
    @NotBlank
    String accountId;
    @NotNull
    @NotBlank
    String displayName;
    @NotNull
    @Min(value = 1, message = "RedmineUserID shouldn't be a minus value")
    Long redmineUserId;

    public User convertToUser() {
        return User.builder()
                .jiraAccountId(accountId)
                .displayName(displayName)
                .redmineUserId(redmineUserId)
                .build();
    }

}
