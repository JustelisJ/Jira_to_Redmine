package com.telesoftas.jira2redmine.api.service.timeentry.session;

import com.telesoftas.jira2redmine.api.mapper.JiraPackageToTimeEntriesMapper;
import com.telesoftas.jira2redmine.api.model.imports.*;
import com.telesoftas.jira2redmine.api.repository.ImportToWorklogRepository;
import com.telesoftas.jira2redmine.api.repository.WorklogRepository;
import com.telesoftas.jira2redmine.api.service.timeentry.TimeEntryService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WorklogUpdater {

    private final TimeEntryService timeEntryService;
    private final JiraPackageToTimeEntriesMapper mapper;
    private final WorklogRepository worklogRepository;
    private final ImportToWorklogRepository importToWorklogRepository;

    public WorklogUpdater(TimeEntryService timeEntryService,
                          JiraPackageToTimeEntriesMapper mapper,
                          WorklogRepository worklogRepository,
                          ImportToWorklogRepository importToWorklogRepository) {
        this.timeEntryService = timeEntryService;
        this.mapper = mapper;
        this.worklogRepository = worklogRepository;
        this.importToWorklogRepository = importToWorklogRepository;
    }

    public void update(List<Worklog> worklogs, ImportStatus importStatus) {
        worklogs.forEach(worklog -> {
            ImportID importID = new ImportID(importStatus, worklog);
            createStatus(importID);
            mapper.mapToTimeEntry(worklog).ifPresentOrElse(timeEntry -> {
                        timeEntryService.updateTimeEntry(timeEntry, worklog.getTimeEntryID());
                        worklogRepository.save(worklog);
                        importToWorklogRepository.updateStatus(importID, ImportToWorklogTableStatus.IMPORTED);
                    },
                    () -> importToWorklogRepository.updateStatus(importID, ImportToWorklogTableStatus.ERROR));
        });
    }

    private void createStatus(ImportID importID) {
        ImportToWorklog importToWorklog = new ImportToWorklog(importID);
        importToWorklogRepository.save(importToWorklog);
    }

}
