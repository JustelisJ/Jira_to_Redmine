package com.telesoftas.jira2redmine.api.service.issue;

import com.telesoftas.jira2redmine.api.model.redminedata.Issue;
import com.telesoftas.jira2redmine.api.model.redminedata.wrappers.IssueData;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Log4j2
@Service
public class RedmineIssueService implements IssueService {

    private static final String MAPPED_ISSUES_MESSAGE = "Mapped all issues";
    private static final String COULDNT_MAP_MESSAGE = "Couldn't map issues";
    public static final String ISSUE_SERVICE_ERROR = "Couldn't fetch issues from Issue service";

    private final String url;
    private final RestTemplate restTemplate;
    private Map<String, Issue> issues;

    public RedmineIssueService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.url = "/issues.json";
        issues = new HashMap<>();
    }

    // post construct disabled because it loads too many records and then fails
    // @PostConstruct
    void postConstruct() {
        mapAllIssues();
    }

    @Override
    public Optional<Issue> getIssueFromName(String name) {
        return Optional.ofNullable(issues.get(name));
    }

    private Set<Issue> getIssues() {
        Set<Issue> issueSet = new HashSet<>();
        String getUrl = url + "?limit={limit}&offset={offset}";

        int totalCount = 0;
        int offset = 0;
        int limit = 100;

        ResponseEntity<IssueData> responseData;
        do {
            Map<String, String> params = new HashMap<>();
            params.put("limit", String.valueOf(limit));
            params.put("offset", String.valueOf(offset));

            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
            HttpEntity<?> entity = new HttpEntity<>(headers);

            responseData = restTemplate.exchange(getUrl, HttpMethod.GET, entity, IssueData.class, params);
            Optional<IssueData> returnedValue = Optional.ofNullable(responseData.getBody());
            if (returnedValue.isPresent()) {
                issueSet.addAll(returnedValue.get().getIssues());
            } else {
                log.error(ISSUE_SERVICE_ERROR);
                break;
            }
            if (totalCount == 0) {
                totalCount = returnedValue.get().getTotalCount();
            }
            offset += returnedValue.get().getIssues().size();
        } while (totalCount != offset);
        return issueSet;
    }

    // Scheduling disabled because it loads too many records and then fails
    // @Scheduled(fixedRate = 300000)
    private void mapAllIssues() {
        Set<Issue> issueSet = getIssues();
        if (!issueSet.isEmpty()) {
            for (Issue issue : issueSet) {
                issues.put(issue.getSubject(), issue);
            }
            log.info(MAPPED_ISSUES_MESSAGE);
        } else {
            log.error(COULDNT_MAP_MESSAGE);
        }
    }
}
