package com.telesoftas.jira2redmine.api.json.writter;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Log4j2
public class FailedPostWriterToFile {

    private static final String FILE_CREATION_MESSAGE = "Incorrent JSON object file is created";

    private final ObjectMapper mapper;

    public FailedPostWriterToFile() {
        this.mapper = new ObjectMapper();
    }

    public void logWrongObjectToFile(Object obj) {
        log.info(FILE_CREATION_MESSAGE);

        try {
            File incorrectObjectFile = File.createTempFile("incorrect_", ".json");

            writingToFile(incorrectObjectFile, obj);
        } catch (IOException ioException) {
            log.error("Error during writing into a file. ", ioException);
        }
    }

    private void writingToFile(File file, Object obj) throws IOException {
        try (FileWriter writer = new FileWriter(file)) {
            writer.write("{\n\"results\":\n");

            //Would have used mapper.writeValue to file, but it just writes to the beginning of the file
            //Does not work if there are any characters in the file
            writer.write(mapper.writeValueAsString(obj));

            writer.write("\n}");
        }
    }

}
