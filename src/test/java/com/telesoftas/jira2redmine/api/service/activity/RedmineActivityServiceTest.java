package com.telesoftas.jira2redmine.api.service.activity;

import com.telesoftas.jira2redmine.api.model.redminedata.Activity;
import com.telesoftas.jira2redmine.api.model.redminedata.wrappers.ActivityData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class RedmineActivityServiceTest {

    private static final long DOCUMENTATION_ID = 4L;
    private static final String ACTIVITY_DOCUMENTATION_NAME = "Documentation";
    private static final String SOME_OTHER_ACTIVITY_NAME = "asdasd";
    private static final long DEFAULT_ID = 5L;

    private static final Activity documentationActivity = Activity.builder()
            .active(true)
            .isDefault(false)
            .id(DOCUMENTATION_ID)
            .name(ACTIVITY_DOCUMENTATION_NAME)
            .build();
    private static final Activity asdasdActivity = Activity.builder()
            .active(true)
            .isDefault(true)
            .id(DEFAULT_ID)
            .name(SOME_OTHER_ACTIVITY_NAME)
            .build();

    @Mock
    RestTemplate restTemplate;

    ActivityService activityService;

    ActivityData activityData;
    ResponseEntity<ActivityData> responseEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        initializeActivityData();
        initializeResponseEntity();
        whenThenForRestTemplate();
        activityService = new RedmineActivityService(restTemplate);
        ((RedmineActivityService) activityService).postConstruct();
    }

    private void whenThenForRestTemplate() {
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), eq(ActivityData.class)))
                .thenReturn(responseEntity);
    }


    private void initializeResponseEntity() {
        responseEntity = new ResponseEntity<>(activityData, HttpStatus.ACCEPTED);
    }

    private void initializeActivityData() {
        activityData = new ActivityData();
        List<Activity> activities = new ArrayList<>();

        activities.add(documentationActivity);
        activities.add(asdasdActivity);

        activityData.setTimeEntryActivities(activities);
    }

    @Test
    void findActivityFromName() {
        Activity activity = activityService.findActivityFromName(ACTIVITY_DOCUMENTATION_NAME);
        assertEquals(documentationActivity, activity);
    }

    @Test
    void dontFindActivityFromName() {
        Activity activity = activityService.findActivityFromName(SOME_OTHER_ACTIVITY_NAME);
        assertEquals(asdasdActivity, activity);
    }
}
