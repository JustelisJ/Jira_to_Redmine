package com.telesoftas.jira2redmine.api.model.imports;

public enum ImportToWorklogTableStatus {

    CREATED, MAPPED, ERROR, IMPORTED

}
