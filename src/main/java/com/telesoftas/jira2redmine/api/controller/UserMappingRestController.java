package com.telesoftas.jira2redmine.api.controller;

import com.telesoftas.jira2redmine.api.model.userMapping.dto.UserDTO;
import com.telesoftas.jira2redmine.api.model.userMapping.entity.User;
import com.telesoftas.jira2redmine.api.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Log4j2
@RestController
public class UserMappingRestController {

    private final UserRepository userRepository;

    public UserMappingRestController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/user/map/save")
    public ResponseEntity<User> saveMapping(@Valid UserDTO user, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(null);
        }

        User dbUser = userRepository.save(user.convertToUser());
        log.info(String.format("User Id %s mapped", user.getAccountId()));
        return ResponseEntity.ok(dbUser);
    }

    @DeleteMapping("/user/map/delete")
    public void deleteMapping(String accountId) {
        userRepository.deleteById(accountId);
        log.info(String.format("User Id %s map deleted", accountId));
    }

}
