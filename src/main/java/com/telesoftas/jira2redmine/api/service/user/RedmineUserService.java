package com.telesoftas.jira2redmine.api.service.user;

import com.telesoftas.jira2redmine.api.model.redminedata.RedmineUser;
import com.telesoftas.jira2redmine.api.model.redminedata.wrappers.UserData;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.*;

@Log4j2
@Service
public class RedmineUserService implements UserService {

    private static final String MAPPED_USERS_MESSAGE = "Mapped all users";
    private static final String COULDNT_MAP_MESSAGE = "Couldn't map users";
    private static final String USER_SERVICE_ERROR = "Couldn't fetch users from RedmineUser service";

    private final String url;
    private final RestTemplate restTemplate;
    private Map<String, RedmineUser> users;
    private Map<Long, String> userIdToName;

    public RedmineUserService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.url = "/users.json";
        users = new HashMap<>();
        userIdToName = new HashMap<>();
    }

    @PostConstruct
    void postConstruct() {
        mapAllUsers();
    }

    @Override
    public Optional<RedmineUser> findUserByName(String fullName) {
        return Optional.ofNullable(users.get(fullName));
    }

    @Override
    public Optional<RedmineUser> findUserById(Long id) {
        return findUserByName(userIdToName.get(id));
    }

    @Override
    public Collection<RedmineUser> getUserList() {
        return users.values();
    }

    private Set<RedmineUser> getUsers() {
        Set<RedmineUser> userSet = new HashSet<>();
        String getUrl = url + "?limit={limit}&offset={offset}";

        int totalCount = 0;
        int offset = 0;
        int limit = 100;

        ResponseEntity<UserData> responseData;
        do {
            Map<String, String> params = new HashMap<>();
            params.put("limit", String.valueOf(limit));
            params.put("offset", String.valueOf(offset));

            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
            HttpEntity<?> entity = new HttpEntity<>(headers);

            responseData = restTemplate.exchange(getUrl, HttpMethod.GET, entity, UserData.class, params);
            Optional<UserData> returnedValue = Optional.ofNullable(responseData.getBody());
            if (returnedValue.isPresent()) {
                userSet.addAll(returnedValue.get().getUsers());
            } else {
                log.error(USER_SERVICE_ERROR);
                break;
            }
            if (totalCount == 0) {
                totalCount = returnedValue.get().getTotalCount();
            }
            offset += returnedValue.get().getUsers().size();
        } while (totalCount != offset);

        return userSet;
    }

    @Scheduled(fixedRate = 300000)
    private void mapAllUsers() {
        Set<RedmineUser> userSet = getUsers();
        if (!userSet.isEmpty()) {
            for (RedmineUser user : userSet) {
                users.put(user.getFullName(), user);
                userIdToName.put(user.getId(), user.getFullName());
            }
            log.info(MAPPED_USERS_MESSAGE);
        } else {
            log.warn(COULDNT_MAP_MESSAGE);
        }
    }
}
