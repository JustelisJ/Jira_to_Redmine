package com.telesoftas.jira2redmine.integration.jiratempo;

import java.io.IOException;
import java.util.Map;

public interface UsersIdToDisplayNameFetcherInterface {
    Map<String, String> fetchAllUsersIdToDisplayName() throws IOException;
}
