package com.telesoftas.jira2redmine.api.comparor;

import com.telesoftas.jira2redmine.api.model.imports.Worklog;
import com.telesoftas.jira2redmine.contract.JiraWorklog;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WorklogComparorTest {

    WorklogComparor worklogComparor;

    Worklog dbWorklog;
    JiraWorklog jiraWorklog;
    JiraWorklog jiraWorklog2;
    JiraWorklog jiraWorklog3;

    @BeforeEach
    void setUp() {
        worklogComparor = new WorklogComparor();

        dbWorklog = Worklog.builder()
                .worklogID(1L)
                .timeEntryID(1L)
                .accountId("account")
                .activity("Work")
                .description("Working on project")
                .issueID("test")
                .projectKey("project")
                .startDate(LocalDate.of(2020, 4, 20))
                .timeSpentSeconds(42069)
                .build();
        jiraWorklog = JiraWorklog.builder()
                .worklogID(1L)
                .accountId("account")
                .activity("Work")
                .description("Working on project")
                .issueID("test")
                .projectKey("project")
                .startDate(LocalDate.of(2020, 4, 20))
                .timeSpentSeconds(42069)
                .build();
        jiraWorklog2 = JiraWorklog.builder()
                .worklogID(2L)
                .accountId("differentAccount")
                .activity("differentWork")
                .description("Working on different project")
                .issueID("differentTest")
                .projectKey("differentProject")
                .startDate(LocalDate.of(2020, 1, 1))
                .timeSpentSeconds(1000)
                .build();
        jiraWorklog3 = JiraWorklog.builder()
                .build();
    }

    @Test
    void worklogsEqual() {
        assertTrue(worklogComparor.worklogsEqual(dbWorklog, jiraWorklog));
    }

    @Test
    void worklogsNotEqual() {
        assertFalse(worklogComparor.worklogsEqual(dbWorklog, jiraWorklog2));
    }

    @Test
    void surprise() {
        assertFalse(worklogComparor.worklogsEqual(dbWorklog, jiraWorklog3));
    }
}