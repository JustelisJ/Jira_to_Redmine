package com.telesoftas.jira2redmine.api.controller;

import com.telesoftas.jira2redmine.api.mapper.JiraPackageToTimeEntriesMapper;
import com.telesoftas.jira2redmine.api.model.imports.ImportStatus;
import com.telesoftas.jira2redmine.api.model.jira.JiraPackage;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.service.timeentry.TimeEntryService;
import com.telesoftas.jira2redmine.api.service.timeentry.session.SessionTimeEntryPost;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.Clock;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;

@Controller
public class TimeEntryController {

    public static final String DISPLAY_TIME_ENTRIES_URL = "timeEntry/displayTimeEntries";

    private final TimeEntryService timeEntryService;
    private final SessionTimeEntryPost asyncPost;
    private final JiraPackageToTimeEntriesMapper mapper;

    public TimeEntryController(TimeEntryService timeEntryService,
                               SessionTimeEntryPost asyncPost, JiraPackageToTimeEntriesMapper mapper) {
        this.timeEntryService = timeEntryService;
        this.asyncPost = asyncPost;
        this.mapper = mapper;
    }

    @GetMapping("/timeEntries")
    public String getIssues(Model model) {
        model.addAttribute("timeEntries", timeEntryService.getTimeEntries());
        return DISPLAY_TIME_ENTRIES_URL;
    }

    @GetMapping("/timeEntries/dateSelection")
    public String pickDateIntervalToFetchWorkLogs(Model model) {
        Clock clock = Clock.systemDefaultZone();
        model.addAttribute("currentMonthStart", YearMonth.now(clock).minusMonths(1).atDay(1));
        model.addAttribute("currentMonthEnd", YearMonth.now(clock).minusMonths(1).atEndOfMonth());
        return "timeEntry/dateInterval";
    }

    @GetMapping("/timeEntries/import")
    public String openAsyncLoadingPage(@ModelAttribute("from") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate from,
                                       @ModelAttribute("to") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate to,
                                       Model model) {
        model.addAttribute("importStatus", asyncPost.importTimeEntries(from, to));
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        return DISPLAY_TIME_ENTRIES_URL;
    }

    @PostMapping("/timeEntries/import")
    public String fetchDateInterval(@ModelAttribute("importStatus") ImportStatus importStatus, Model model) {
        if (asyncPost.checkIfImportIsComplete(importStatus)) {
            List<TimeEntry> postResult = asyncPost.fetchCompletePost(importStatus);

            int totalPosts = asyncPost.importedWorklogCount(importStatus);
            model.addAttribute("timeEntries", postResult);
            model.addAttribute("postedFromAll", String.format("Total imports: %d/%d", postResult.size(), totalPosts));
        }
        return "timeEntry/timeEntryTable";
    }

    @PostMapping("/timeEntries/new")
    public String createTimeEntryFromJSON(@RequestBody JiraPackage jiraPackage) {

        List<TimeEntry> timeEntries = mapper.mapToTimeEntries(jiraPackage);
        if (!timeEntries.isEmpty()) {
            timeEntryService.postTimeEntries(timeEntries);
        }
        return "redirect:/timeEntries";
    }
}
