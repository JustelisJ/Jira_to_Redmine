package com.telesoftas.jira2redmine.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telesoftas.jira2redmine.api.mapper.JiraPackageToTimeEntriesMapper;
import com.telesoftas.jira2redmine.api.mapper.JiraUsersToRedmineUsersMapper;
import com.telesoftas.jira2redmine.api.model.jira.JiraPackage;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.repository.UserRepository;
import com.telesoftas.jira2redmine.api.service.activity.ActivityService;
import com.telesoftas.jira2redmine.api.service.issue.IssueService;
import com.telesoftas.jira2redmine.api.service.timeentry.TimeEntryService;
import com.telesoftas.jira2redmine.api.service.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.List;

@Log4j2
@RequiredArgsConstructor
@Component
public class ImportWorklogsOnBootup implements ApplicationRunner {

    public static final String OPTION_NAME = "import-worklogs-file";

    private final TimeEntryService entryService;

    private final IssueService issueService;

    private final UserService userService;

    private final ActivityService activityService;

    private final UserRepository userRepository;

    public void run(ApplicationArguments args) throws Exception {
        if (!args.containsOption(OPTION_NAME)) {
            return;
        }
        String dataUrl = args.getOptionValues(OPTION_NAME).get(0);
        // read json and post on service
        ObjectMapper mapper = new ObjectMapper();
        InputStream inputStream = resolveDataUrl(dataUrl);
        try {
            JiraPackage jiraPackage = mapper.readValue(inputStream, JiraPackage.class);
            JiraPackageToTimeEntriesMapper timeEntriesMapper = new JiraPackageToTimeEntriesMapper(
                    issueService, activityService, new JiraUsersToRedmineUsersMapper(userService), userRepository, userService);
            List<TimeEntry> timeEntries = timeEntriesMapper.mapToTimeEntries(jiraPackage);
            if (!timeEntries.isEmpty()) {
                entryService.postTimeEntries(timeEntries);
                log.info("Work logs Saved!");
            }
        } catch (IOException e) {
            log.error("Unable to save work logs", e);
        }
    }

    private InputStream resolveDataUrl(String dataUrl) {
        if ("-".equals(dataUrl)) {
            return System.in;
        }

        try {
            return new FileInputStream(new File(dataUrl));
        } catch (FileNotFoundException e) {
            // ignore
        }

        return getClass().getResourceAsStream(dataUrl);
    }
}
