package com.telesoftas.jira2redmine.api.model.userMapping.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
public class User {
    @Id
    String jiraAccountId;
    String displayName;

    @Column(unique = true)
    Long redmineUserId;

}
