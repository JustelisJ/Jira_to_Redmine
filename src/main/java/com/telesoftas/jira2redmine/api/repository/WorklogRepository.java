package com.telesoftas.jira2redmine.api.repository;

import com.telesoftas.jira2redmine.api.model.imports.Worklog;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface WorklogRepository extends CrudRepository<Worklog, Long> {

    Optional<Worklog> findWorklogByWorklogID(Long worklogID);

}
