package com.telesoftas.jira2redmine.api.model.redminedata.wrappers;

import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TimeEntryData extends Metadata {

    private List<TimeEntry> timeEntries;

}
