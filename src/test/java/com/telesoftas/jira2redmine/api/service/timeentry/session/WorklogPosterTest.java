package com.telesoftas.jira2redmine.api.service.timeentry.session;

import com.telesoftas.jira2redmine.api.mapper.JiraPackageToTimeEntriesMapper;
import com.telesoftas.jira2redmine.api.model.imports.*;
import com.telesoftas.jira2redmine.api.model.redminedata.TimeEntry;
import com.telesoftas.jira2redmine.api.repository.ImportToWorklogRepository;
import com.telesoftas.jira2redmine.api.repository.WorklogRepository;
import com.telesoftas.jira2redmine.api.service.timeentry.TimeEntryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class WorklogPosterTest {

    @Mock
    TimeEntryService timeEntryService;
    @Mock
    JiraPackageToTimeEntriesMapper mapper;
    @Mock
    WorklogRepository worklogRepository;
    @Mock
    ImportToWorklogRepository importToWorklogRepository;

    WorklogPoster poster;

    ImportStatus importStatus;
    List<Worklog> worklogs;
    Worklog dbWorklog;
    TimeEntry timeEntry;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        poster = new WorklogPoster(timeEntryService, mapper, worklogRepository, importToWorklogRepository);

        importStatus = new ImportStatus(1L, ImportStatusTableStatus.PENDING);
        dbWorklog = Worklog.builder()
                .worklogID(1L)
                .timeEntryID(2L)
                .build();
        worklogs = new ArrayList<>();
        worklogs.add(dbWorklog);
        timeEntry = TimeEntry.builder()
                .id(2L)
                .build();
    }

    @Test
    void post_failToMap() {
        when(mapper.mapToTimeEntry(any(Worklog.class))).thenReturn(Optional.empty());

        poster.post(worklogs, importStatus);

        verify(timeEntryService, times(0)).postTimeEntry(any(TimeEntry.class));
        verify(worklogRepository, times(0)).save(any(Worklog.class));
        verify(importToWorklogRepository, times(1))
                .updateStatus(any(ImportID.class), eq(ImportToWorklogTableStatus.ERROR));
    }

    @Test
    void post_failsToPost() {
        when(mapper.mapToTimeEntry(any(Worklog.class))).thenReturn(Optional.of(timeEntry));
        when(timeEntryService.postTimeEntry(any(TimeEntry.class))).thenReturn(Optional.empty());

        poster.post(worklogs, importStatus);

        verify(timeEntryService, times(1)).postTimeEntry(any(TimeEntry.class));
        verify(worklogRepository, times(0)).save(any(Worklog.class));
        verify(importToWorklogRepository, times(1))
                .updateStatus(any(ImportID.class), eq(ImportToWorklogTableStatus.ERROR));
    }

    @Test
    void post_postsSuccessfully() {
        when(mapper.mapToTimeEntry(any(Worklog.class))).thenReturn(Optional.of(timeEntry));
        when(timeEntryService.postTimeEntry(any(TimeEntry.class))).thenReturn(Optional.of(timeEntry));
        when(worklogRepository.save(any(Worklog.class))).thenReturn(dbWorklog);

        poster.post(worklogs, importStatus);

        verify(timeEntryService, times(1)).postTimeEntry(any(TimeEntry.class));
        verify(worklogRepository, times(1)).save(any(Worklog.class));
        verify(importToWorklogRepository, times(1))
                .updateStatus(any(ImportID.class), eq(ImportToWorklogTableStatus.IMPORTED));
    }
}